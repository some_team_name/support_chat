<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Message'), ['action' => 'edit', $message->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Message'), ['action' => 'delete', $message->id], ['confirm' => __('Are you sure you want to delete # {0}?', $message->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Messages'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Message'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Chats Users'), ['controller' => 'ChatsUsers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Chats User'), ['controller' => 'ChatsUsers', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Chats'), ['controller' => 'Chats', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Chat'), ['controller' => 'Chats', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="messages view large-9 medium-8 columns content">
    <h3><?= h($message->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Chats User') ?></th>
            <td><?= $message->has('chats_user') ? $this->Html->link($message->chats_user->id, ['controller' => 'ChatsUsers', 'action' => 'view', $message->chats_user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Chat') ?></th>
            <td><?= $message->has('chat') ? $this->Html->link($message->chat->id, ['controller' => 'Chats', 'action' => 'view', $message->chat->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($message->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Chat Visitor Id') ?></th>
            <td><?= $this->Number->format($message->chat_visitor_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($message->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($message->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Body') ?></h4>
        <?= $this->Text->autoParagraph(h($message->body)); ?>
    </div>
</div>
