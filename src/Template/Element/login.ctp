<?=$this->Html->css([
    'style.min.css'],
     ['block' => true]
);
?>
<!-- begin login -->
<div class="login bg-black animated fadeInDown">
    <!-- begin brand -->
    <div class="login-header">
        <div class="brand">
            <span class="logo"></span> Chat Admin
            <!-- TODO: delete <small>responsive bootstrap 3 admin template</small> -->
        </div>
        <div class="icon">
            <i class="fa fa-sign-in"></i>
        </div>
    </div>
    <!-- end brand -->
    <div class="login-content">
        <?= $this->Form->create(null, ['url' => ['controller' => 'Users', 'action' => 'login'], 'class' => 'margin-bottom-0']) ?>
        <?php 
        $myTemplates = [
            'inputContainer' => '<div class="form-group m-b-20 input">{{content}}</div>',
        ];
        $this->Form->templates($myTemplates);
        ?>

        <?= $this->Form->input('email', ['label'=>false, 'class' => 'form-control input-lg', 'placeholder'=>'Email Address']) ?>
        <?= $this->Form->input('password', ['label'=>false, 'class' => 'form-control input-lg', 'placeholder'=>'Password']) ?>
            <div class="checkbox i-checks">
                <label> 
                    <div class="checkbox_square-green">
                        <?= $this->Form->checkbox('remember_me', ['label' => true]) ?>
                        <ins class="check-helper"></ins>
                    </div>
                    <i></i>
                     Remember Me
                </label>
            </div>
        
        <div class="login-buttons">
            <?= $this->Form->button('Login', ['class' => 'btn btn-success btn-block btn-lg']) ?>
        </div> 
        <?= $this->Form->end() ?>
         <div class="form-group m-t-30">
            <div class="col-sm-7">
                <?= $this->Html->link('<i class="fa fa-lock m-r-5"></i> ' . __('Forgot your
                    password?'). '</a>', ['controller' => 'Users', 'action' => 'recoverpwd'], ['class'=>'text-muted', 'escape' => false]) ?>
            </div>
            <div class="col-sm-5 text-right">
                <?= $this->Html->link(__('Create an account'), ['controller' => 'Users', 'action' => 'register'], ['class'=>'text-muted']) ?>
            </div>
        </div>
    </div>
</div>
<!-- end login -->
