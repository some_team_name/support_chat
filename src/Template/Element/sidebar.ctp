<!-- begin #sidebar -->
<div id="sidebar" class="sidebar">
	<!-- begin sidebar scrollbar -->
	<div data-scrollbar="true" data-height="100%">
			<!-- begin sidebar user -->
			<ul class="nav">
				<li class="nav-profile">
					<div class="image">
						<a href="javascript:;"><?=$this->Html->image('users/user-13.jpg');?></a>
					</div>
					<div class="info">
						<?=$user->email?>
						<small><?=$user->role?></small>
					</div>
				</li>
			</ul>
			<!-- end sidebar user -->
			<!-- begin sidebar nav -->
			<ul class="nav">
				<li class="nav-header">Navigation</li>
				
				<li class="active">
					<?= $this->Html->link('<i class="fa fa-laptop"></i>
						<span>Dashboard</span>', ['controller' => 'Admin'], ['escape' => false]); ?>
				</li>


				<li class="has-sub menu-hosts">
					<a href="javascript:;">
					    <!-- <b class="caret pull-right"></b> -->
					    <i class="fa fa-globe"></i>
					    <span>Sites</span>
						<span class="badge pull-right hosts-counter"><?=count($user->account->hosts)?></span>
				    </a>
					<ul class="sub-menu">
						<li class="active sites-list">
							<?= $this->Html->link('Sites list', ['controller' => 'Admin', 'action'=>'sites']); ?>
					    </li>
					    <?php foreach($user->account->hosts as $hostItem): ?>
						    <li>
						    <?= $this->Html->link($hostItem->host, ['controller'=>'Admin', 'action'=>'site', $hostItem->id]);?>
						    </li>
					    <?php endforeach; ?>
					</ul>
				</li>
				<li class="has-sub menu-users">
					<?= $this->Html->link('<i class="fa fa-users"></i> 
						<span>Users</span>
						<span class="badge pull-right users-counter">'.count($user->account->users).'</span>', ['controller' => 'Admin', 'action' => 'users'], ['escape' => false]); ?>
				</li>
			
		        <!-- begin sidebar minify button -->
				<li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
		        <!-- end sidebar minify button -->
			</ul>
			<!-- end sidebar nav -->
		</div>
	<!-- end sidebar scrollbar -->

</div>
<div class="sidebar-bg"></div>
<!-- end #sidebar -->
	
