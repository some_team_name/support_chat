<?php
$this->Html->css([
    'http://seantheme.com/color-admin-v2.0/admin/html/assets/css/style.min.css'],
     ['block' => true]
);
?>

<style type="text/css">
    .container {
        width: initial;
    }
</style>

<div id="content">
    <div id="main">
        <div id="page-container" class="fade in">
        <!-- begin login -->
        <div class="login bg-black animated fadeInDown">
            <!-- begin brand -->
            <div class="login-header">
                <div class="brand">
                    <span class="logo"></span> Chat Admin
                    <!-- TODO: delete <small>responsive bootstrap 3 admin template</small> -->
                </div>
                <div class="icon">
                    <i class="fa fa-sign-in"></i>
                </div>
            </div>
            <!-- end brand -->
            <div class="login-content">
                <?= $this->Form->create(null, ['url' => ['controller' => 'Users', 'action' => 'register'], 'class' => 'margin-bottom-0']) ?>
                <?php 
                $myTemplates = [
                    'inputContainer' => '<div class="form-group m-b-20 input">{{content}}</div>',
                ];
                $this->Form->templates($myTemplates);
                ?>

                <?= $this->Form->input('email', ['label'=>false, 'class' => 'form-control input-lg', 'placeholder'=>__('Email Address')]) ?>
                <?= $this->Form->input('password', ['label'=>false, 'class' => 'form-control input-lg', 'placeholder'=>__('Password')]) ?>
                <?= $this->Form->input('password_confirm', ['label'=>false, 'class' => 'form-control input-lg', 'placeholder'=> __('Сonfirm password')]) ?>
               <div class="form-group">
                    <div class="checkbox i-checks">
                        <label> 
                            <div class="checkbox_square-green">
                                <?= $this->Form->checkbox('agree_terms') ?>

                                <ins class="check-helper"></ins>
                            </div>
                            <i></i>
                             Agree the terms and policy 
                        </label>
                    </div>
                </div>
                <div class="login-buttons">
                    <?= $this->Form->button(__('Register'), ['class' => 'btn btn-success btn-block btn-lg']) ?>
                </div> 
                <?= $this->Form->end() ?>
                <div class="form-group m-t-30">
                    <div class="col-sm-12 text-center">
                        <?= $this->Html->link(__('Already have an account?'), ['controller' => 'Users', 'action' => 'login'], ['class'=>'text-muted']) ?>
                    </div>
                </div>

            </div>
        </div>
        <!-- end login -->
        
        
    </div>
    </div>
</div>
<script>
    $(document).ready(function(){
            function fix_height(){
                    var h = $("#tray").height();   
                    $("#preview").attr("height", (($(window).height()) - h) + "px");
            }
            $(window).resize(function(){ fix_height(); }).resize();
            //$("#preview").contentWindow.focus();
    });
</script>
