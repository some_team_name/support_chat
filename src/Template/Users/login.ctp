<style type="text/css">
    .container {
        width: initial;
    }
</style>

<div id="content">
    <div id="main">
        <div id="page-container" class="fade in">
        <?= $this->element('login'); ?>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
            function fix_height(){
                    var h = $("#tray").height();   
                    $("#preview").attr("height", (($(window).height()) - h) + "px");
            }
            $(window).resize(function(){ fix_height(); }).resize();
            //$("#preview").contentWindow.focus();
    });
</script>
