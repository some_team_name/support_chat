<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $chatsVisitor->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $chatsVisitor->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Chats Visitors'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Visitors'), ['controller' => 'Visitors', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Visitor'), ['controller' => 'Visitors', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Chats'), ['controller' => 'Chats', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Chat'), ['controller' => 'Chats', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Hosts'), ['controller' => 'Hosts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Host'), ['controller' => 'Hosts', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="chatsVisitors form large-9 medium-8 columns content">
    <?= $this->Form->create($chatsVisitor) ?>
    <fieldset>
        <legend><?= __('Edit Chats Visitor') ?></legend>
        <?php
            echo $this->Form->input('visitor_id', ['options' => $visitors, 'empty' => true]);
            echo $this->Form->input('chat_id', ['options' => $chats]);
            echo $this->Form->input('host_id', ['options' => $hosts, 'empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
