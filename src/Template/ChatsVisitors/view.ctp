<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Chats Visitor'), ['action' => 'edit', $chatsVisitor->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Chats Visitor'), ['action' => 'delete', $chatsVisitor->id], ['confirm' => __('Are you sure you want to delete # {0}?', $chatsVisitor->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Chats Visitors'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Chats Visitor'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Visitors'), ['controller' => 'Visitors', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Visitor'), ['controller' => 'Visitors', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Chats'), ['controller' => 'Chats', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Chat'), ['controller' => 'Chats', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Hosts'), ['controller' => 'Hosts', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Host'), ['controller' => 'Hosts', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="chatsVisitors view large-9 medium-8 columns content">
    <h3><?= h($chatsVisitor->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Visitor') ?></th>
            <td><?= $chatsVisitor->has('visitor') ? $this->Html->link($chatsVisitor->visitor->id, ['controller' => 'Visitors', 'action' => 'view', $chatsVisitor->visitor->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Chat') ?></th>
            <td><?= $chatsVisitor->has('chat') ? $this->Html->link($chatsVisitor->chat->id, ['controller' => 'Chats', 'action' => 'view', $chatsVisitor->chat->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Host') ?></th>
            <td><?= $chatsVisitor->has('host') ? $this->Html->link($chatsVisitor->host->id, ['controller' => 'Hosts', 'action' => 'view', $chatsVisitor->host->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($chatsVisitor->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($chatsVisitor->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($chatsVisitor->modified) ?></td>
        </tr>
    </table>
</div>
