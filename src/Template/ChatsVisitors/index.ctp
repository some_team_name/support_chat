<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Chats Visitor'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Visitors'), ['controller' => 'Visitors', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Visitor'), ['controller' => 'Visitors', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Chats'), ['controller' => 'Chats', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Chat'), ['controller' => 'Chats', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Hosts'), ['controller' => 'Hosts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Host'), ['controller' => 'Hosts', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="chatsVisitors index large-9 medium-8 columns content">
    <h3><?= __('Chats Visitors') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('visitor_id') ?></th>
                <th><?= $this->Paginator->sort('chat_id') ?></th>
                <th><?= $this->Paginator->sort('host_id') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($chatsVisitors as $chatsVisitor): ?>
            <tr>
                <td><?= $this->Number->format($chatsVisitor->id) ?></td>
                <td><?= $chatsVisitor->has('visitor') ? $this->Html->link($chatsVisitor->visitor->id, ['controller' => 'Visitors', 'action' => 'view', $chatsVisitor->visitor->id]) : '' ?></td>
                <td><?= $chatsVisitor->has('chat') ? $this->Html->link($chatsVisitor->chat->id, ['controller' => 'Chats', 'action' => 'view', $chatsVisitor->chat->id]) : '' ?></td>
                <td><?= $chatsVisitor->has('host') ? $this->Html->link($chatsVisitor->host->id, ['controller' => 'Hosts', 'action' => 'view', $chatsVisitor->host->id]) : '' ?></td>
                <td><?= h($chatsVisitor->created) ?></td>
                <td><?= h($chatsVisitor->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $chatsVisitor->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $chatsVisitor->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $chatsVisitor->id], ['confirm' => __('Are you sure you want to delete # {0}?', $chatsVisitor->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
