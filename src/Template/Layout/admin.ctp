<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Online chat';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    <meta name="viewport" content="width=1024">
    <!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <?php /*$this->Html->css('//d85wutc1n854v.cloudfront.net/live/css/screen_preview.css')*/ ?>
    <?= $this->Html->css('http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700') ?>
    <?= $this->Html->css('jquery-ui/themes/base/jquery-ui.min.css') ?>
    <?= $this->Html->css('bootstrap/bootstrap.min.css') ?>
    <?= $this->Html->css('font-awesome.min.css') ?>
    <?= $this->Html->css('animate.min.css') ?>
    <?= $this->Html->css('style-responsive.min.css') ?>
    <?= $this->Html->css('purple.css') ?>
    <?=$this->Html->css([
        'style.min.css'
        , 'theme/default.css'
        ,'gritter/jquery.gritter.css']
    );?>


    <?= $this->Html->css('style.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <?= $this->Html->script('pace.min.js') ?>
    <?= $this->Html->script('script.js') ?>
    <!-- <?= $this->Html->script('//d85wutc1n854v.cloudfront.net/live/js/behavior.js') ?> -->
    <?= $this->fetch('script') ?>
</head>
<body>
        <?= $this->Flash->render() ?>
    <section class="clearfix">
        

        <div id="page-container" class="fade page-sidebar-fixed page-header-fixed in">
            <?= $this->element('header'); ?>        
            <?= $this->element('sidebar'); ?>        
            
            <!-- begin #content -->
            <div id="content" class="content">
                <?= $this->fetch('content') ?>
            </div>
            <!-- end #content -->
            <?= ''/*$this->element('theme-panel');*/ ?>        
            
            <!-- begin scroll to top btn -->
            <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
            <!-- end scroll to top btn -->
        </div>
        <?=$this->Html->script([
            'http://seantheme.com/color-admin-v2.0/admin/html/assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js'
            , 'http://seantheme.com/color-admin-v2.0/admin/html/assets/plugins/bootstrap/js/bootstrap.min.js'
            , 'http://seantheme.com/color-admin-v2.0/admin/html/assets/plugins/slimscroll/jquery.slimscroll.min.js'
            , 'http://seantheme.com/color-admin-v2.0/admin/html/assets/plugins/jquery-cookie/jquery.cookie.js'
            , 'http://seantheme.com/color-admin-v2.0/admin/html/assets/plugins/morris/raphael.min.js'
            /*, 'http://seantheme.com/color-admin-v2.0/admin/html/assets/plugins/morris/morris.js'*/
            , 'http://seantheme.com/color-admin-v2.0/admin/html/assets/plugins/gritter/js/jquery.gritter.js'
            , 'dashboard-v2.min.js'
            , 'http://seantheme.com/color-admin-v2.0/admin/html/assets/js/apps.min.js'
            ]
        );?>
        <script>
            $(document).ready(function() {
                App.init();
                DashboardV2.init();
            });
        </script>
    </section>
    <footer>
    </footer>
</body>
</html>
