<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Chat'), ['action' => 'edit', $chat->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Chat'), ['action' => 'delete', $chat->id], ['confirm' => __('Are you sure you want to delete # {0}?', $chat->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Chats'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Chat'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Hosts'), ['controller' => 'Hosts', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Host'), ['controller' => 'Hosts', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Chat Settings'), ['controller' => 'ChatSettings', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Chat Setting'), ['controller' => 'ChatSettings', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Messages'), ['controller' => 'Messages', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Message'), ['controller' => 'Messages', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Visitors'), ['controller' => 'Visitors', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Visitor'), ['controller' => 'Visitors', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="chats view large-9 medium-8 columns content">
    <h3><?= h($chat->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Host') ?></th>
            <td><?= $chat->has('host') ? $this->Html->link($chat->host->id, ['controller' => 'Hosts', 'action' => 'view', $chat->host->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($chat->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($chat->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($chat->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Chat Settings') ?></h4>
        <?php if (!empty($chat->chat_settings)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Chat Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($chat->chat_settings as $chatSettings): ?>
            <tr>
                <td><?= h($chatSettings->chat_id) ?></td>
                <td><?= h($chatSettings->created) ?></td>
                <td><?= h($chatSettings->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'ChatSettings', 'action' => 'view', $chatSettings->chat_id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'ChatSettings', 'action' => 'edit', $chatSettings->chat_id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ChatSettings', 'action' => 'delete', $chatSettings->chat_id], ['confirm' => __('Are you sure you want to delete # {0}?', $chatSettings->chat_id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Messages') ?></h4>
        <?php if (!empty($chat->messages)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Chat User Id') ?></th>
                <th><?= __('Chat Id') ?></th>
                <th><?= __('Body') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($chat->messages as $messages): ?>
            <tr>
                <td><?= h($messages->id) ?></td>
                <td><?= h($messages->chat_user_id) ?></td>
                <td><?= h($messages->chat_id) ?></td>
                <td><?= h($messages->body) ?></td>
                <td><?= h($messages->created) ?></td>
                <td><?= h($messages->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Messages', 'action' => 'view', $messages->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Messages', 'action' => 'edit', $messages->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Messages', 'action' => 'delete', $messages->id], ['confirm' => __('Are you sure you want to delete # {0}?', $messages->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Users') ?></h4>
        <?php if (!empty($chat->users)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Email') ?></th>
                <th><?= __('Password') ?></th>
                <th><?= __('Role') ?></th>
                <th><?= __('Account Id') ?></th>
                <th><?= __('Account Owner') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($chat->users as $users): ?>
            <tr>
                <td><?= h($users->id) ?></td>
                <td><?= h($users->email) ?></td>
                <td><?= h($users->password) ?></td>
                <td><?= h($users->role) ?></td>
                <td><?= h($users->account_id) ?></td>
                <td><?= h($users->account_owner) ?></td>
                <td><?= h($users->created) ?></td>
                <td><?= h($users->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Users', 'action' => 'view', $users->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Users', 'action' => 'edit', $users->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Users', 'action' => 'delete', $users->id], ['confirm' => __('Are you sure you want to delete # {0}?', $users->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Visitors') ?></h4>
        <?php if (!empty($chat->visitors)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Host Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($chat->visitors as $visitors): ?>
            <tr>
                <td><?= h($visitors->id) ?></td>
                <td><?= h($visitors->host_id) ?></td>
                <td><?= h($visitors->created) ?></td>
                <td><?= h($visitors->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Visitors', 'action' => 'view', $visitors->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Visitors', 'action' => 'edit', $visitors->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Visitors', 'action' => 'delete', $visitors->id], ['confirm' => __('Are you sure you want to delete # {0}?', $visitors->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
</div>
