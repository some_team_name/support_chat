<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $visitor->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $visitor->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Visitors'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Hosts'), ['controller' => 'Hosts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Host'), ['controller' => 'Hosts', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Chats Users'), ['controller' => 'ChatsUsers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Chats User'), ['controller' => 'ChatsUsers', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="visitors form large-9 medium-8 columns content">
    <?= $this->Form->create($visitor) ?>
    <fieldset>
        <legend><?= __('Edit Visitor') ?></legend>
        <?php
            echo $this->Form->input('host_id', ['options' => $hosts]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
