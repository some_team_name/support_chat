<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Visitor'), ['action' => 'edit', $visitor->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Visitor'), ['action' => 'delete', $visitor->id], ['confirm' => __('Are you sure you want to delete # {0}?', $visitor->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Visitors'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Visitor'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Hosts'), ['controller' => 'Hosts', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Host'), ['controller' => 'Hosts', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Chats Users'), ['controller' => 'ChatsUsers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Chats User'), ['controller' => 'ChatsUsers', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="visitors view large-9 medium-8 columns content">
    <h3><?= h($visitor->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Host') ?></th>
            <td><?= $visitor->has('host') ? $this->Html->link($visitor->host->id, ['controller' => 'Hosts', 'action' => 'view', $visitor->host->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($visitor->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($visitor->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($visitor->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Chats Users') ?></h4>
        <?php if (!empty($visitor->chats_users)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Chat Id') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Host Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($visitor->chats_users as $chatsUsers): ?>
            <tr>
                <td><?= h($chatsUsers->id) ?></td>
                <td><?= h($chatsUsers->chat_id) ?></td>
                <td><?= h($chatsUsers->user_id) ?></td>
                <td><?= h($chatsUsers->host_id) ?></td>
                <td><?= h($chatsUsers->created) ?></td>
                <td><?= h($chatsUsers->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'ChatsUsers', 'action' => 'view', $chatsUsers->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'ChatsUsers', 'action' => 'edit', $chatsUsers->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ChatsUsers', 'action' => 'delete', $chatsUsers->id], ['confirm' => __('Are you sure you want to delete # {0}?', $chatsUsers->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
</div>
