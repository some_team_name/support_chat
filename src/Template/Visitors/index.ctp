<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Visitor'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Hosts'), ['controller' => 'Hosts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Host'), ['controller' => 'Hosts', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Chats Users'), ['controller' => 'ChatsUsers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Chats User'), ['controller' => 'ChatsUsers', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="visitors index large-9 medium-8 columns content">
    <h3><?= __('Visitors') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('host_id') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($visitors as $visitor): ?>
            <tr>
                <td><?= $this->Number->format($visitor->id) ?></td>
                <td><?= $visitor->has('host') ? $this->Html->link($visitor->host->id, ['controller' => 'Hosts', 'action' => 'view', $visitor->host->id]) : '' ?></td>
                <td><?= h($visitor->created) ?></td>
                <td><?= h($visitor->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $visitor->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $visitor->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $visitor->id], ['confirm' => __('Are you sure you want to delete # {0}?', $visitor->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
