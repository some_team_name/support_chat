<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Chats User'), ['action' => 'edit', $chatsUser->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Chats User'), ['action' => 'delete', $chatsUser->id], ['confirm' => __('Are you sure you want to delete # {0}?', $chatsUser->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Chats Users'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Chats User'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Visitors'), ['controller' => 'Visitors', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Visitor'), ['controller' => 'Visitors', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Hosts'), ['controller' => 'Hosts', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Host'), ['controller' => 'Hosts', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Chats'), ['controller' => 'Chats', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Chat'), ['controller' => 'Chats', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="chatsUsers view large-9 medium-8 columns content">
    <h3><?= h($chatsUser->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Chat') ?></th>
            <td><?= $chatsUser->has('chat') ? $this->Html->link($chatsUser->chat->id, ['controller' => 'Chats', 'action' => 'view', $chatsUser->chat->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $chatsUser->has('user') ? $this->Html->link($chatsUser->user->id, ['controller' => 'Users', 'action' => 'view', $chatsUser->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Host') ?></th>
            <td><?= $chatsUser->has('host') ? $this->Html->link($chatsUser->host->id, ['controller' => 'Hosts', 'action' => 'view', $chatsUser->host->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($chatsUser->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($chatsUser->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($chatsUser->modified) ?></td>
        </tr>
    </table>
</div>
