<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Chats User'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Visitors'), ['controller' => 'Visitors', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Visitor'), ['controller' => 'Visitors', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Hosts'), ['controller' => 'Hosts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Host'), ['controller' => 'Hosts', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Chats'), ['controller' => 'Chats', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Chat'), ['controller' => 'Chats', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="chatsUsers index large-9 medium-8 columns content">
    <h3><?= __('Chats Users') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('chat_id') ?></th>
                <th><?= $this->Paginator->sort('user_id') ?></th>
                <th><?= $this->Paginator->sort('host_id') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($chatsUsers as $chatsUser): ?>
            <tr>
                <td><?= $this->Number->format($chatsUser->id) ?></td>
                <td><?= $chatsUser->has('chat') ? $this->Html->link($chatsUser->chat->id, ['controller' => 'Chats', 'action' => 'view', $chatsUser->chat->id]) : '' ?></td>
                <td><?= $chatsUser->has('user') ? $this->Html->link($chatsUser->user->id, ['controller' => 'Users', 'action' => 'view', $chatsUser->user->id]) : '' ?></td>
                <td><?= $chatsUser->has('host') ? $this->Html->link($chatsUser->host->id, ['controller' => 'Hosts', 'action' => 'view', $chatsUser->host->id]) : '' ?></td>
                <td><?= h($chatsUser->created) ?></td>
                <td><?= h($chatsUser->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $chatsUser->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $chatsUser->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $chatsUser->id], ['confirm' => __('Are you sure you want to delete # {0}?', $chatsUser->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
