<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $hostSetting->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $hostSetting->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Host Settings'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Hosts'), ['controller' => 'Hosts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Host'), ['controller' => 'Hosts', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="hostSettings form large-9 medium-8 columns content">
    <?= $this->Form->create($hostSetting) ?>
    <fieldset>
        <legend><?= __('Edit Host Setting') ?></legend>
        <?php
            echo $this->Form->input('host_id', ['options' => $hosts]);
            echo $this->Form->input('lang');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
