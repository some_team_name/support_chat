<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Host Setting'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Hosts'), ['controller' => 'Hosts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Host'), ['controller' => 'Hosts', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="hostSettings index large-9 medium-8 columns content">
    <h3><?= __('Host Settings') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('host_id') ?></th>
                <th><?= $this->Paginator->sort('lang') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($hostSettings as $hostSetting): ?>
            <tr>
                <td><?= $this->Number->format($hostSetting->id) ?></td>
                <td><?= $hostSetting->has('host') ? $this->Html->link($hostSetting->host->id, ['controller' => 'Hosts', 'action' => 'view', $hostSetting->host->id]) : '' ?></td>
                <td><?= h($hostSetting->lang) ?></td>
                <td><?= h($hostSetting->created) ?></td>
                <td><?= h($hostSetting->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $hostSetting->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $hostSetting->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $hostSetting->id], ['confirm' => __('Are you sure you want to delete # {0}?', $hostSetting->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
