<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Host Setting'), ['action' => 'edit', $hostSetting->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Host Setting'), ['action' => 'delete', $hostSetting->id], ['confirm' => __('Are you sure you want to delete # {0}?', $hostSetting->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Host Settings'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Host Setting'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Hosts'), ['controller' => 'Hosts', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Host'), ['controller' => 'Hosts', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="hostSettings view large-9 medium-8 columns content">
    <h3><?= h($hostSetting->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Host') ?></th>
            <td><?= $hostSetting->has('host') ? $this->Html->link($hostSetting->host->id, ['controller' => 'Hosts', 'action' => 'view', $hostSetting->host->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Lang') ?></th>
            <td><?= h($hostSetting->lang) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($hostSetting->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($hostSetting->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($hostSetting->modified) ?></td>
        </tr>
    </table>
</div>
