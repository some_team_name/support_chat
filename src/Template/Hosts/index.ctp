<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Host'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Accounts'), ['controller' => 'Accounts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Account'), ['controller' => 'Accounts', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Chats'), ['controller' => 'Chats', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Chat'), ['controller' => 'Chats', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Chats Users'), ['controller' => 'ChatsUsers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Chats User'), ['controller' => 'ChatsUsers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Host Settings'), ['controller' => 'HostSettings', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Host Setting'), ['controller' => 'HostSettings', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Visitors'), ['controller' => 'Visitors', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Visitor'), ['controller' => 'Visitors', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="hosts index large-9 medium-8 columns content">
    <h3><?= __('Hosts') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('host') ?></th>
                <th><?= $this->Paginator->sort('account_id') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($hosts as $host): ?>
            <tr>
                <td><?= $this->Number->format($host->id) ?></td>
                <td><?= h($host->host) ?></td>
                <td><?= $host->has('account') ? $this->Html->link($host->account->id, ['controller' => 'Accounts', 'action' => 'view', $host->account->id]) : '' ?></td>
                <td><?= h($host->created) ?></td>
                <td><?= h($host->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $host->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $host->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $host->id], ['confirm' => __('Are you sure you want to delete # {0}?', $host->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
