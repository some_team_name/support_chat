<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Host'), ['action' => 'edit', $host->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Host'), ['action' => 'delete', $host->id], ['confirm' => __('Are you sure you want to delete # {0}?', $host->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Hosts'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Host'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Accounts'), ['controller' => 'Accounts', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Account'), ['controller' => 'Accounts', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Chats'), ['controller' => 'Chats', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Chat'), ['controller' => 'Chats', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Chats Users'), ['controller' => 'ChatsUsers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Chats User'), ['controller' => 'ChatsUsers', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Host Settings'), ['controller' => 'HostSettings', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Host Setting'), ['controller' => 'HostSettings', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Visitors'), ['controller' => 'Visitors', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Visitor'), ['controller' => 'Visitors', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="hosts view large-9 medium-8 columns content">
    <h3><?= h($host->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Host') ?></th>
            <td><?= h($host->host) ?></td>
        </tr>
        <tr>
            <th><?= __('Account') ?></th>
            <td><?= $host->has('account') ? $this->Html->link($host->account->id, ['controller' => 'Accounts', 'action' => 'view', $host->account->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($host->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($host->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($host->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Chats') ?></h4>
        <?php if (!empty($host->chats)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Host Id') ?></th>
                <th><?= __('Status') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($host->chats as $chats): ?>
            <tr>
                <td><?= h($chats->id) ?></td>
                <td><?= h($chats->host_id) ?></td>
                <td><?= h($chats->status) ?></td>
                <td><?= h($chats->created) ?></td>
                <td><?= h($chats->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Chats', 'action' => 'view', $chats->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Chats', 'action' => 'edit', $chats->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Chats', 'action' => 'delete', $chats->id], ['confirm' => __('Are you sure you want to delete # {0}?', $chats->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Chats Users') ?></h4>
        <?php if (!empty($host->chats_users)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Chat Id') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Host Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($host->chats_users as $chatsUsers): ?>
            <tr>
                <td><?= h($chatsUsers->id) ?></td>
                <td><?= h($chatsUsers->chat_id) ?></td>
                <td><?= h($chatsUsers->user_id) ?></td>
                <td><?= h($chatsUsers->host_id) ?></td>
                <td><?= h($chatsUsers->created) ?></td>
                <td><?= h($chatsUsers->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'ChatsUsers', 'action' => 'view', $chatsUsers->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'ChatsUsers', 'action' => 'edit', $chatsUsers->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ChatsUsers', 'action' => 'delete', $chatsUsers->id], ['confirm' => __('Are you sure you want to delete # {0}?', $chatsUsers->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Host Settings') ?></h4>
        <?php if (!empty($host->host_settings)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Host Id') ?></th>
                <th><?= __('Lang') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($host->host_settings as $hostSettings): ?>
            <tr>
                <td><?= h($hostSettings->id) ?></td>
                <td><?= h($hostSettings->host_id) ?></td>
                <td><?= h($hostSettings->lang) ?></td>
                <td><?= h($hostSettings->created) ?></td>
                <td><?= h($hostSettings->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'HostSettings', 'action' => 'view', $hostSettings->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'HostSettings', 'action' => 'edit', $hostSettings->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'HostSettings', 'action' => 'delete', $hostSettings->id], ['confirm' => __('Are you sure you want to delete # {0}?', $hostSettings->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Visitors') ?></h4>
        <?php if (!empty($host->visitors)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Host Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($host->visitors as $visitors): ?>
            <tr>
                <td><?= h($visitors->id) ?></td>
                <td><?= h($visitors->host_id) ?></td>
                <td><?= h($visitors->created) ?></td>
                <td><?= h($visitors->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Visitors', 'action' => 'view', $visitors->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Visitors', 'action' => 'edit', $visitors->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Visitors', 'action' => 'delete', $visitors->id], ['confirm' => __('Are you sure you want to delete # {0}?', $visitors->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Users') ?></h4>
        <?php if (!empty($host->users)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Email') ?></th>
                <th><?= __('Password') ?></th>
                <th><?= __('Role') ?></th>
                <th><?= __('Account Id') ?></th>
                <th><?= __('Account Owner') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($host->users as $users): ?>
            <tr>
                <td><?= h($users->id) ?></td>
                <td><?= h($users->email) ?></td>
                <td><?= h($users->password) ?></td>
                <td><?= h($users->role) ?></td>
                <td><?= h($users->account_id) ?></td>
                <td><?= h($users->account_owner) ?></td>
                <td><?= h($users->created) ?></td>
                <td><?= h($users->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Users', 'action' => 'view', $users->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Users', 'action' => 'edit', $users->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Users', 'action' => 'delete', $users->id], ['confirm' => __('Are you sure you want to delete # {0}?', $users->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
</div>
