<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/*
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;
*/
?>

<style type="text/css">
    .container {
        width: initial;
        padding-right: 0; 
        padding-left: 0; 
    }
</style>

<div id="content" class="main-page">
    <div id="main">
        <div id="page-container" class="fade in">
            <div class="news-feed">
                <div class="news-image">
                    <img src="http://seantheme.com/color-admin-v2.0/admin/html/assets/img/login-bg/bg-8.jpg" alt="">
                </div>
                <div class="news-caption">
                    <h4 class="caption-title"><i class="fa fa-edit text-success"></i> Announcing the Color Admin app</h4>
                    <p>
                        As a Color Admin Apps administrator, you use the Color Admin console to manage your organization’s account, such as add new users, manage security settings, and turn on the services you want your team to access.
                    </p>
                </div>
            </div>  
            <?= $this->element('login'); ?>        
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
            function fix_height(){
                    var h = $("#tray").height();   
                    $("#preview").attr("height", (($(window).height()) - h) + "px");
            }
            $(window).resize(function(){ fix_height(); }).resize();
            //$("#preview").contentWindow.focus();
    });
</script>
