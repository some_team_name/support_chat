<h3><?= __('Sites') ?></h3>
<?= $this->Form->create(null, ['url' => ['controller' => 'Hosts', 'action' => 'add'], 'class'=>'form-inline site-add-form']) ?>
    <?= $this->Form->input('host',  ['label'=>false, 'value'=>'', 'type' => 'text', 'class'=>'form-control', 'placeholder' => __('Add site'), 'templates' => ['inputContainer'=>'<div class="col-md-8 form-group m-r-10">{{content}}</div>']]); ?>

<?=$this->Html->link(__('Add site'), 'javascript:;', ['class' => 'btn btn-sm btn-primary m-r-5 save-btn']) ?>
<?= $this->Form->end() ?>

<div class="panel panel-inverse" data-sortable-id="table-basic-7">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand" data-original-title="" title=""><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload" data-original-title="" title=""><i class="fa fa-repeat"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse" data-original-title="" title=""><i class="fa fa-minus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove" data-original-title="" title=""><i class="fa fa-times"></i></a>
        </div>
        <h4 class="panel-title"><?=__('Sites list')?></h4>
    </div>
    <div class="panel-body sites-list" style="display: block;">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr>
						<th data-col-type="number">#</th>
						<th data-col-type="host">Host</th>
						<th data-col-type="offline">Host status</th><!-- TODO: add request to site (per hour) to detect online/offline -->
						<th data-col-type="0">Chats active</th>
						<th data-col-type="0/0">Operators (all/online)</th>
						<th data-col-type="id">Site id</th>
                		<th data-col-type="actions" class="actions"><?= __('Actions') ?></th>
					</tr>
				</thead>
				<tbody>
						    <?php /*$this->Html->link($host->host, ['controller'=>'Admin', 'action'=>'site', $host->id]);*/ 
						    $hostsArr = $hosts->toArray();
						    unset($hosts);
						    ?>
					<?php foreach($user->account->hosts as $key => $hostItem): ?>
						<tr>
							<td><?=$key+1?></td>
							<td><?=$hostItem->host?></td>
							<!-- TODO: add feature like on http://secure.wp-chat.com/chat with switcher -->
							<td>Online</td> 
							<td><?=$hostsArr[$key]['activeChats']; /*print_r($hosts, 1);*/ ?></td>
							<!-- TODO: add count on user rights to host and active users -->
							<td><?=count($user->account->users) . '/'.count($user->account->users)?></td> 
							<td><?=$hostItem->id?></td>
							<td class="actions">
								<i class="fa fa-pencil-square-o"></i>
								<i class="fa fa-trash"></i>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
