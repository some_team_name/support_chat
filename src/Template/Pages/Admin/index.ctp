		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="javascript:;">Home</a></li>
			<li><a href="javascript:;">Dashboard</a></li>
			<li class="active">Dashboard v2</li>
		</ol>
		<!-- end breadcrumb -->
		<!-- begin page-header -->
		<h1 class="page-header">Dashboard v2 <small>header small text goes here...</small></h1>
		<!-- end page-header -->
		<!-- begin row -->
		<div class="row">
		    <!-- begin col-3 -->
		    <div class="col-md-3 col-sm-6">
		        <div class="widget widget-stats bg-green">
		            <div class="stats-icon stats-icon-lg"><i class="fa fa-globe fa-fw"></i></div>
		            <div class="stats-title">TODAY'S VISITS</div>
		            <div class="stats-number">7,842,900</div>
		            <div class="stats-progress progress">
                        <div class="progress-bar" style="width: 70.1%;"></div>
                    </div>
                    <div class="stats-desc">Better than last week (70.1%)</div>
		        </div>
		    </div>
		    <!-- end col-3 -->
		    <!-- begin col-3 -->
		    <div class="col-md-3 col-sm-6">
		        <div class="widget widget-stats bg-blue">
		            <div class="stats-icon stats-icon-lg"><i class="fa fa-tags fa-fw"></i></div>
		            <div class="stats-title">TODAY'S PROFIT</div>
		            <div class="stats-number">180,200</div>
		            <div class="stats-progress progress">
                        <div class="progress-bar" style="width: 40.5%;"></div>
                    </div>
                    <div class="stats-desc">Better than last week (40.5%)</div>
		        </div>
		    </div>
		    <!-- end col-3 -->
		    <!-- begin col-3 -->
		    <div class="col-md-3 col-sm-6">
		        <div class="widget widget-stats bg-purple">
		            <div class="stats-icon stats-icon-lg"><i class="fa fa-shopping-cart fa-fw"></i></div>
		            <div class="stats-title">NEW ORDERS</div>
		            <div class="stats-number">38,900</div>
		            <div class="stats-progress progress">
                        <div class="progress-bar" style="width: 76.3%;"></div>
                    </div>
                    <div class="stats-desc">Better than last week (76.3%)</div>
		        </div>
		    </div>
		    <!-- end col-3 -->
		    <!-- begin col-3 -->
		    <div class="col-md-3 col-sm-6">
		        <div class="widget widget-stats bg-black">
		            <div class="stats-icon stats-icon-lg"><i class="fa fa-comments fa-fw"></i></div>
		            <div class="stats-title">NEW COMMENTS</div>
		            <div class="stats-number">3,988</div>
		            <div class="stats-progress progress">
                        <div class="progress-bar" style="width: 54.9%;"></div>
                    </div>
                    <div class="stats-desc">Better than last week (54.9%)</div>
		        </div>
		    </div>
		    <!-- end col-3 -->
		</div>
		<!-- end row -->		
		
		<!-- begin row -->
		<div class="row">
		    <!-- begin col-4 -->
		    <div class="col-md-4 ui-sortable">
		        <!-- begin panel -->
		        <div class="panel panel-inverse" data-sortable-id="index-2">
		            <div class="panel-heading">
		                <h4 class="panel-title">Chat History <span class="label label-success pull-right">4 message</span></h4>
		            </div>
		            <div class="panel-body bg-silver">
                        <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 225px;">
                        <div data-scrollbar="true" data-height="225px" data-init="true" style="overflow: hidden; width: auto; height: 225px;">
                            <ul class="chats">
                                <li class="left">
                                    <span class="date-time">yesterday 11:23pm</span>
                                    <a href="javascript:;" class="name">Sowse Bawdy</a>
                                    <a href="javascript:;" class="image"><?=$this->Html->image('users/user-12.jpg');?></a>
                                    <div class="message">
                                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit volutpat. Praesent mattis interdum arcu eu feugiat.
                                    </div>
                                </li>
                                <li class="right">
                                    <span class="date-time">08:12am</span>
                                    <a href="#" class="name"><span class="label label-primary">ADMIN</span> Me</a>
                                    <a href="javascript:;" class="image"><?=$this->Html->image('users/user-13.jpg');?></a>
                                    <div class="message">
                                        Nullam posuere, nisl a varius rhoncus, risus tellus hendrerit neque.
                                    </div>
                                </li>
                                <li class="left">
                                    <span class="date-time">09:20am</span>
                                    <a href="#" class="name">Neck Jolly</a>
                                    <a href="javascript:;" class="image"><?=$this->Html->image('users/user-10.jpg');?></a>
                                    <div class="message">
                                        Euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
                                    </div>
                                </li>
                                <li class="left">
                                    <span class="date-time">11:15am</span>
                                    <a href="#" class="name">Shag Strap</a>
                                    <a href="javascript:;" class="image"><?=$this->Html->image('users/user-14.jpg');?></a>
                                    <div class="message">
                                        Nullam iaculis pharetra pharetra. Proin sodales tristique sapien mattis placerat.
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="slimScrollBar ui-draggable" style="width: 7px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px; height: 75.2229px; background: rgb(0, 0, 0);"></div>
                        <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; opacity: 0.2; z-index: 90; right: 1px; background: rgb(51, 51, 51);"></div></div>
                    </div>
                    <div class="panel-footer">
                        <form name="send_message_form" data-id="message-form">
                            <div class="input-group">
                                <input type="text" class="form-control input-sm" name="message" placeholder="Enter your message here.">
                                <span class="input-group-btn">
                                    <button class="btn btn-primary btn-sm" type="button">Send</button>
                                </span>
                            </div>
                        </form>
                    </div>
		        </div>
		        <!-- end panel -->
		    </div>
		    <!-- end col-4 -->
		    <!-- begin col-4 -->
		    <div class="col-md-4 ui-sortable">
		        <!-- begin panel -->
		        <div class="panel panel-inverse" data-sortable-id="index-4">
		            <div class="panel-heading">
		                <h4 class="panel-title">New Registered Users <span class="pull-right label label-success">24 new users</span></h4>
		            </div>
                    <ul class="registered-users-list clearfix">
                        <li>
                            <a href="javascript:;"><?=$this->Html->image('users/user-5.jpg');?></a>
                            <h4 class="username text-ellipsis">
                                Savory Posh
                                <small>Algerian</small>
                            </h4>
                        </li>
                        <li>
                            <a href="javascript:;"><?=$this->Html->image('users/user-3.jpg');?></a>
                            <h4 class="username text-ellipsis">
                                Ancient Caviar
                                <small>Korean</small>
                            </h4>
                        </li>
                        <li>
                            <a href="javascript:;"><?=$this->Html->image('users/user-1.jpg');?></a>
                            <h4 class="username text-ellipsis">
                                Marble Lungs
                                <small>Indian</small>
                            </h4>
                        </li>
                        <li>
                            <a href="javascript:;"><?=$this->Html->image('users/user-8.jpg');?></a>
                            <h4 class="username text-ellipsis">
                                Blank Bloke
                                <small>Japanese</small>
                            </h4>
                        </li>
                        <li>
                            <a href="javascript:;"><?=$this->Html->image('users/user-2.jpg');?></a>
                            <h4 class="username text-ellipsis">
                                Hip Sculling
                                <small>Cuban</small>
                            </h4>
                        </li>
                        <li>
                            <a href="javascript:;"><?=$this->Html->image('users/user-6.jpg');?></a>
                            <h4 class="username text-ellipsis">
                                Flat Moon
                                <small>Nepalese</small>
                            </h4>
                        </li>
                        <li>
                            <a href="javascript:;"><?=$this->Html->image('users/user-4.jpg');?></a>
                            <h4 class="username text-ellipsis">
                                Packed Puffs
                                <small>Malaysian&gt;</small>
                            </h4>
                        </li>
                        <li>
                            <a href="javascript:;"><?=$this->Html->image('users/user-9.jpg');?></a>
                            <h4 class="username text-ellipsis">
                                Clay Hike
                                <small>Swedish</small>
                            </h4>
                        </li>
                    </ul>
		            <div class="panel-footer text-center">
		                <a href="javascript:;" class="text-inverse">View All</a>
		            </div>
		        </div>
		        <!-- end panel -->
		    </div>
		    <!-- end col-4 -->
		</div>
		<!-- end row -->
