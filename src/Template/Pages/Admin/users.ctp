<h3><?= __('Users') ?></h3>
    <!-- Email, password, role. Not account owner -->
    <?= $this->Html->link(__('Add'), '#modal-message', ['class' => 'btn btn-sm btn-primary', 'data-toggle'=>'modal']) ?>

<div class="panel panel-inverse" data-sortable-id="table-basic-7">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand" data-original-title="" title=""><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload" data-original-title="" title=""><i class="fa fa-repeat"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse" data-original-title="" title=""><i class="fa fa-minus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove" data-original-title="" title=""><i class="fa fa-times"></i></a>
        </div>
        <h4 class="panel-title"><?=__('Users list')?></h4>
    </div>
    <div class="panel-body users-list" style="display: block;">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr>
						<th data-col-type="number">#</th>
						<th data-col-type="email">Email</th>
						<th data-col-type="role">Role</th>
						<th data-col-type="offline">User status</th><!-- TODO: add request to site (per hour) to detect online/offline -->
						<th data-col-type="">Chats active</th>
						<th data-col-type="0">Hosts</th>
						<th data-col-type="id">User id</th>
                		<th data-col-type="actions" class="actions"><?= __('Actions') ?></th>
					</tr>
				</thead>
				<tbody>
						    <?php /*$this->Html->link($host->host, ['controller'=>'Admin', 'action'=>'site', $host->id]);*/ 
						    // $hostsArr = $hosts->toArray();
						    // unset($hosts);
						    ?>
					<?php foreach($user->account->users as $key => $userItem): ?>
						<tr>
							<td><?=$key+1?></td>
							<td><?=$userItem->email?></td>
							<td><?=$userItem->role?></td>
							<td>Online</td> <!-- TODO: add feature like on http://secure.wp-chat.com/chat with switcher -->
							<td><?=''/*$hostsArr[$key]['activeChats']; print_r($hosts, 1);*/ ?></td>
							<td><?=count($user->account->hosts)?></td> 
							<td><?=$userItem->id?></td>
							<td class="actions">
								<i class="fa fa-pencil-square-o"></i>
								<i class="fa fa-trash"></i>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<!-- #modal-message -->
<div class="modal modal-message fade" id="modal-message">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<!-- <h4 class="modal-title">Add user</h4> -->
			</div>
			<?= $this->Form->create(null, ['id'=>'add-user', 'class'=>'form-horizontal form-bordered', 'url' =>[
				    "controller" => "Users",
				    "action" => "add"
				]]) ?>
			<div class="modal-body">
				<div class="users form large-9 medium-8 columns content">
				    <fieldset>
				        <legend><?= __('Add User') ?></legend>
				        <?php
				        	use App\Model\Entity\User;

				            echo $this->Form->input('email', ['class' => 'form-control',
				            		'label'=>['class'=>'control-label']
				            		, 'required' => true]);
				            echo $this->Form->input('password', ['class' => 'form-control',
				            		'label'=>['class'=>'control-label']
				            		, 'required' => true]);
				            echo $this->Form->input('role', ['class' => 'form-control',
				            		'label'=>['class'=>'control-label']
				            		, 'required' => true,
				            	'options'=> [
					            	User::ADMIN => __('Administrator'),
					            	User::OPERATOR =>  __('Moderator')
				            	]]);
				        ?>
				    </fieldset>
				</div>
			</div>
			<div class="modal-footer">
				<a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">Close</a>
				<?=$this->Html->link(__('Save user'), 'javascript:;', ['class' => 'btn btn-sm btn-primary save-btn']) ?>
			</div>
			<?= $this->Form->end() ?>
		</div>
	</div>
</div>
<!-- #modal-alert -->
