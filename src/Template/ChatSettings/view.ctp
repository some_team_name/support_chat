<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Chat Setting'), ['action' => 'edit', $chatSetting->chat_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Chat Setting'), ['action' => 'delete', $chatSetting->chat_id], ['confirm' => __('Are you sure you want to delete # {0}?', $chatSetting->chat_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Chat Settings'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Chat Setting'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Chats'), ['controller' => 'Chats', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Chat'), ['controller' => 'Chats', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="chatSettings view large-9 medium-8 columns content">
    <h3><?= h($chatSetting->chat_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Chat') ?></th>
            <td><?= $chatSetting->has('chat') ? $this->Html->link($chatSetting->chat->id, ['controller' => 'Chats', 'action' => 'view', $chatSetting->chat->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($chatSetting->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($chatSetting->modified) ?></td>
        </tr>
    </table>
</div>
