<?php
namespace App\Model\Table;

use App\Model\Entity\Chat;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

//TODO: add `status` ENUM('open','close','archive')

/**
 * Chats Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Hosts
 * @property \Cake\ORM\Association\HasMany $ChatSettings
 * @property \Cake\ORM\Association\HasMany $Messages
 * @property \Cake\ORM\Association\BelongsToMany $Users
 * @property \Cake\ORM\Association\BelongsToMany $Visitors
 */
class ChatsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('chats');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Hosts', [
            'foreignKey' => 'host_id'
        ]);
        $this->hasMany('ChatSettings', [
            'foreignKey' => 'chat_id'
        ]);
        $this->hasMany('Messages', [
            'foreignKey' => 'chat_id'
        ]);
        $this->belongsToMany('Users', [
            'foreignKey' => 'chat_id',
            'targetForeignKey' => 'user_id',
            'joinTable' => 'chats_users'
        ]);
        $this->belongsToMany('Visitors', [
            'foreignKey' => 'chat_id',
            'targetForeignKey' => 'visitor_id',
            'joinTable' => 'chats_visitors'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['host_id'], 'Hosts'));
        return $rules;
    }

    public function setMessageSender($message)
    {
        $chat = $this->get($message->chat_id, [ 
            'contain' => ['Users', 'Visitors']
        ]);

        if (!empty($message->visitor_id)) {
            $visitorId = (int) $message->visitor_id;
            unset($message->visitor_id);

            foreach ($chat->visitors as $visitor) {
                if ($visitor->id == $visitorId) {
                    $message->chat_visitor_id = $visitorId;
                    return true;
                }
            }
        } elseif(!empty($message->user_id)) {
            $userId = (int) $message->user_id;
            unset($message->user_id);

            foreach ($chat->users as $user) {
                if ($user->id == $userId) {
                    $message->chat_user_id = $userId;
                    return true;
                }
            }

            $message->chat_user_id = $userId; //TODO: fix it - invite new user to chat or if have no users in chat
            return true;

        } else {
            return false;
            // TODO: changed to http error
        }
    }

    public function getUserChats($userId)
    {
        $user = $this->Users->get($userId, [
            'contain' => ['Accounts']
        ]);

        if (is_null($user)) return false;

        $hosts = $this->Hosts->find()
            ->where(['Hosts.account_id' => $user->account_id])->contain(['Chats'  => function ($q) {
                   return $q
                        ->where(['Chats.status' => 'open']);
                }])
            ->first();
        return $hosts->chats;
    }

    public function setHostId(&$chat) {
        $hostId = null;

        unset($chat->host_id);

        if (!empty($_SERVER['HTTP_REFERER'])) {
            $hostStr = $this->Hosts->getURLHost($_SERVER['HTTP_REFERER']);
            $host = $this->Hosts->find()
                ->where(['Hosts.host' => $hostStr])
                ->first();

            if (!$host) {
                throw new \Exception("Have no $hostStr in hosts base"); // TODO: handle exception
            }

            $hostId = $host->id;
        } elseif (!empty($this->request->data['host_id'])) {
            // TODO: add getting host_id from app key
            $hostId = (int) $this->request->data['host_id'];
        }

        if (is_null($hostId)) {
            throw new \Exception("Host id is empty"); // TODO: handle exception
        }

        $chat->host_id = $hostId;
    }
}
