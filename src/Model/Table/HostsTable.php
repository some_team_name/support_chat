<?php
namespace App\Model\Table;

use App\Model\Entity\Host;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Hosts Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Accounts
 * @property \Cake\ORM\Association\HasMany $Chats
 * @property \Cake\ORM\Association\HasMany $ChatsUsers
 * @property \Cake\ORM\Association\HasMany $ChatsVisitors
 * @property \Cake\ORM\Association\HasMany $HostSettings
 * @property \Cake\ORM\Association\HasMany $Visitors
 * @property \Cake\ORM\Association\BelongsToMany $Users
 */
class HostsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('hosts');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Accounts', [
            'foreignKey' => 'account_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Chats', [
            'foreignKey' => 'host_id'
        ]);
        $this->hasMany('ChatsUsers', [
            'foreignKey' => 'host_id'
        ]);
        $this->hasMany('ChatsVisitors', [
            'foreignKey' => 'host_id'
        ]);
        $this->hasMany('HostSettings', [
            'foreignKey' => 'host_id'
        ]);
        $this->hasMany('Visitors', [
            'foreignKey' => 'host_id'
        ]);
        $this->belongsToMany('Users', [
            'foreignKey' => 'host_id',
            'targetForeignKey' => 'user_id',
            'joinTable' => 'hosts_users'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('host', 'create')
            ->notEmpty('host');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['host']));
        $rules->add($rules->existsIn(['account_id'], 'Accounts'));
        return $rules;
    }


    public function getURLHost($host) {
        $url = strtolower(trim($host));
        if (strpos($url, '://') === false) $url = 'http://'.trim($url, '/:');
        $url = parse_url($url, PHP_URL_HOST);
        if ($url[strlen($url) - 1] == '/') $url = substr($url, 0, -1);
        if (substr($url, 0, 4) == "www.") $url = substr($url, 4);
        return $url;
    }
}
