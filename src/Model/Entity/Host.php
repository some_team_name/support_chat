<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Host Entity.
 *
 * @property int $id
 * @property string $host
 * @property int $account_id
 * @property \App\Model\Entity\Account $account
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property \App\Model\Entity\Chat[] $chats
 * @property \App\Model\Entity\ChatsUser[] $chats_users
 * @property \App\Model\Entity\HostSetting[] $host_settings
 * @property \App\Model\Entity\Visitor[] $visitors
 */
class Host extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
