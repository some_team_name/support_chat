<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ChatSettings Controller
 *
 * @property \App\Model\Table\ChatSettingsTable $ChatSettings
 */
class ChatSettingsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Chats']
        ];
        $chatSettings = $this->paginate($this->ChatSettings);

        $this->set(compact('chatSettings'));
        $this->set('_serialize', ['chatSettings']);
    }

    /**
     * View method
     *
     * @param string|null $id Chat Setting id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $chatSetting = $this->ChatSettings->get($id, [
            'contain' => ['Chats']
        ]);

        $this->set('chatSetting', $chatSetting);
        $this->set('_serialize', ['chatSetting']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $chatSetting = $this->ChatSettings->newEntity();
        if ($this->request->is('post')) {
            $chatSetting = $this->ChatSettings->patchEntity($chatSetting, $this->request->data);
            if ($this->ChatSettings->save($chatSetting)) {
                $this->Flash->success(__('The chat setting has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The chat setting could not be saved. Please, try again.'));
            }
        }
        $chats = $this->ChatSettings->Chats->find('list', ['limit' => 200]);
        $this->set(compact('chatSetting', 'chats'));
        $this->set('_serialize', ['chatSetting']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Chat Setting id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $chatSetting = $this->ChatSettings->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $chatSetting = $this->ChatSettings->patchEntity($chatSetting, $this->request->data);
            if ($this->ChatSettings->save($chatSetting)) {
                $this->Flash->success(__('The chat setting has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The chat setting could not be saved. Please, try again.'));
            }
        }
        $chats = $this->ChatSettings->Chats->find('list', ['limit' => 200]);
        $this->set(compact('chatSetting', 'chats'));
        $this->set('_serialize', ['chatSetting']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Chat Setting id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $chatSetting = $this->ChatSettings->get($id);
        if ($this->ChatSettings->delete($chatSetting)) {
            $this->Flash->success(__('The chat setting has been deleted.'));
        } else {
            $this->Flash->error(__('The chat setting could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
