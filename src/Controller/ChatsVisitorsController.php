<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ChatsVisitors Controller
 *
 * @property \App\Model\Table\ChatsVisitorsTable $ChatsVisitors
 */
class ChatsVisitorsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Visitors', 'Chats', 'Hosts']
        ];
        $chatsVisitors = $this->paginate($this->ChatsVisitors);

        $this->set(compact('chatsVisitors'));
        $this->set('_serialize', ['chatsVisitors']);
    }

    /**
     * View method
     *
     * @param string|null $id Chats Visitor id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $chatsVisitor = $this->ChatsVisitors->get($id, [
            'contain' => ['Visitors', 'Chats', 'Hosts']
        ]);

        $this->set('chatsVisitor', $chatsVisitor);
        $this->set('_serialize', ['chatsVisitor']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $chatsVisitor = $this->ChatsVisitors->newEntity();
        if ($this->request->is('post')) {
            $chatsVisitor = $this->ChatsVisitors->patchEntity($chatsVisitor, $this->request->data);
            if ($this->ChatsVisitors->save($chatsVisitor)) {
                $this->Flash->success(__('The chats visitor has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The chats visitor could not be saved. Please, try again.'));
            }
        }
        $visitors = $this->ChatsVisitors->Visitors->find('list', ['limit' => 200]);
        $chats = $this->ChatsVisitors->Chats->find('list', ['limit' => 200]);
        $hosts = $this->ChatsVisitors->Hosts->find('list', ['limit' => 200]);
        $this->set(compact('chatsVisitor', 'visitors', 'chats', 'hosts'));
        $this->set('_serialize', ['chatsVisitor']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Chats Visitor id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $chatsVisitor = $this->ChatsVisitors->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $chatsVisitor = $this->ChatsVisitors->patchEntity($chatsVisitor, $this->request->data);
            if ($this->ChatsVisitors->save($chatsVisitor)) {
                $this->Flash->success(__('The chats visitor has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The chats visitor could not be saved. Please, try again.'));
            }
        }
        $visitors = $this->ChatsVisitors->Visitors->find('list', ['limit' => 200]);
        $chats = $this->ChatsVisitors->Chats->find('list', ['limit' => 200]);
        $hosts = $this->ChatsVisitors->Hosts->find('list', ['limit' => 200]);
        $this->set(compact('chatsVisitor', 'visitors', 'chats', 'hosts'));
        $this->set('_serialize', ['chatsVisitor']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Chats Visitor id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $chatsVisitor = $this->ChatsVisitors->get($id);
        if ($this->ChatsVisitors->delete($chatsVisitor)) {
            $this->Flash->success(__('The chats visitor has been deleted.'));
        } else {
            $this->Flash->error(__('The chats visitor could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
