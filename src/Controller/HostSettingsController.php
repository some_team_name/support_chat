<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * HostSettings Controller
 *
 * @property \App\Model\Table\HostSettingsTable $HostSettings
 */
class HostSettingsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Hosts']
        ];
        $hostSettings = $this->paginate($this->HostSettings);

        $this->set(compact('hostSettings'));
        $this->set('_serialize', ['hostSettings']);
    }

    /**
     * View method
     *
     * @param string|null $id Host Setting id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $hostSetting = $this->HostSettings->get($id, [
            'contain' => ['Hosts']
        ]);

        $this->set('hostSetting', $hostSetting);
        $this->set('_serialize', ['hostSetting']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $hostSetting = $this->HostSettings->newEntity();
        if ($this->request->is('post')) {
            $hostSetting = $this->HostSettings->patchEntity($hostSetting, $this->request->data);
            if ($this->HostSettings->save($hostSetting)) {
                $this->Flash->success(__('The host setting has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The host setting could not be saved. Please, try again.'));
            }
        }
        $hosts = $this->HostSettings->Hosts->find('list', ['limit' => 200]);
        $this->set(compact('hostSetting', 'hosts'));
        $this->set('_serialize', ['hostSetting']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Host Setting id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $hostSetting = $this->HostSettings->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $hostSetting = $this->HostSettings->patchEntity($hostSetting, $this->request->data);
            if ($this->HostSettings->save($hostSetting)) {
                $this->Flash->success(__('The host setting has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The host setting could not be saved. Please, try again.'));
            }
        }
        $hosts = $this->HostSettings->Hosts->find('list', ['limit' => 200]);
        $this->set(compact('hostSetting', 'hosts'));
        $this->set('_serialize', ['hostSetting']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Host Setting id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $hostSetting = $this->HostSettings->get($id);
        if ($this->HostSettings->delete($hostSetting)) {
            $this->Flash->success(__('The host setting has been deleted.'));
        } else {
            $this->Flash->error(__('The host setting could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
