<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Model\Entity\User;
use Cake\Event\Event;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['logout']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Accounts']
        ];
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Accounts', 'Chats', 'Profiles']
        ]);

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $curUserId = $this->Auth->user('id');
        if (is_null($curUserId)) return; // TODO: throw an error

        $curUser = $this->Users->get($curUserId);
        if (!$curUser->isAdmin()) return; // TODO: throw an rights error

        $user = $this->Users->newEntity($this->request->data);
        $user->account_owner = false;
        $user->account_id = $curUser->account_id;

        if ($this->Users->save($user)) {
            $message = ['success' => __('The user has been saved.')];
            // return $this->redirect(['action' => 'index']);
        } else {
            $message = ['error' => __('The user could not be saved. Please, try again.')];
        }

        
        $this->set([
            'message' => $message,
            'user' => $user,
            '_serialize' => ['message', 'user']
        ]);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Chats']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $accounts = $this->Users->Accounts->find('list', ['limit' => 200]);
        $chats = $this->Users->Chats->find('list', ['limit' => 200]);
        $this->set(compact('user', 'accounts', 'chats'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function login()
    {
        if ($this->request->is('post')) {
            // TODO: detect if remember_me - set long cookie, if not - only for browser session
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error('Your username or password is incorrect.');
        } else {
            // var_dump($this->Auth->config('loginRedirect'));
        }
    }

    public function logout()
    {
        $this->Flash->success('You are now logged out.');
        $this->Auth->logout();
        return $this->redirect($this->Auth->config('logoutRedirect'));
    }

    public function recoverpwd()
    {
        if (!$this->request->is('post')) return;
        $user = $this->Users->findByEmail($this->request->data['email'])->first();
        // TODO: add recovery password
    }

    public function register()
    {
        if (!$this->request->is('post')) return;
        $data = $this->request->data;
        $user = $this->Users->findByEmail($data['email'])->first();
        if ($user) {
            $this->Flash->error(__('The user could not be registered.') . ' ' . __('Email exists.'));
            return;
        }

        if ($data['password'] !== $data['password_confirm']) {
            $this->Flash->error(__('The user could not be registered.') . ' ' . __('Password and confirm password mismatch'));
            return;
        }
        //TODO: add detecting as password strong

        $account = $this->Users->Accounts->newEntity($this->request->data);
        $user = $this->Users->newEntity($this->request->data);
        
        // TODO: rewrite claases for the REST (not only is POST)
        if ($this->Users->Accounts->save($account)) {
            $user->account_id = $account->id;
            $user->account_owner = true;
            $user->role = User::ADMIN;
            if ($this->Users->Accounts->Users->save($user)) {
                $message = __('The user has been registered.');
                $this->Flash->success($message);
                $this->Auth->setUser($user->toArray());
                // TODO: if ajax - return register data
                return $this->redirect($this->Auth->redirectUrl()); // TODO: add redirect to main admin page;
            } else {
                $this->Flash->error(__('The user could not be created. Please, try again.'));
            }
        } else {
            $message = __('The account could not be saved. Please, try again.');
            $this->Flash->error($message);
        }
    }

    public function beforeFilter(Event $event) {
        $this->Auth->config('logoutRedirect', '/');
        $this->Auth->config('loginRedirect', [
            'controller' => 'Admin',
            'action' => 'index'
        ]);

        parent::beforeFilter($event);
    }
}
