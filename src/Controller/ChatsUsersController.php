<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ChatsUsers Controller
 *
 * @property \App\Model\Table\ChatsUsersTable $ChatsUsers
 */
class ChatsUsersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Visitors', 'Hosts', 'Chats']
        ];
        $chatsUsers = $this->paginate($this->ChatsUsers);

        $this->set(compact('chatsUsers'));
        $this->set('_serialize', ['chatsUsers']);
    }

    /**
     * View method
     *
     * @param string|null $id Chats User id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $chatsUser = $this->ChatsUsers->get($id, [
            'contain' => ['Users', 'Visitors', 'Hosts', 'Chats']
        ]);

        $this->set('chatsUser', $chatsUser);
        $this->set('_serialize', ['chatsUser']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $chatsUser = $this->ChatsUsers->newEntity();
        if ($this->request->is('post')) {
            $chatsUser = $this->ChatsUsers->patchEntity($chatsUser, $this->request->data);
            if ($this->ChatsUsers->save($chatsUser)) {
                $this->Flash->success(__('The chats user has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The chats user could not be saved. Please, try again.'));
            }
        }
        $users = $this->ChatsUsers->Users->find('list', ['limit' => 200]);
        $visitors = $this->ChatsUsers->Visitors->find('list', ['limit' => 200]);
        $hosts = $this->ChatsUsers->Hosts->find('list', ['limit' => 200]);
        $chats = $this->ChatsUsers->Chats->find('list', ['limit' => 200]);
        $this->set(compact('chatsUser', 'users', 'visitors', 'hosts', 'chats'));
        $this->set('_serialize', ['chatsUser']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Chats User id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $chatsUser = $this->ChatsUsers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $chatsUser = $this->ChatsUsers->patchEntity($chatsUser, $this->request->data);
            if ($this->ChatsUsers->save($chatsUser)) {
                $this->Flash->success(__('The chats user has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The chats user could not be saved. Please, try again.'));
            }
        }
        $users = $this->ChatsUsers->Users->find('list', ['limit' => 200]);
        $visitors = $this->ChatsUsers->Visitors->find('list', ['limit' => 200]);
        $hosts = $this->ChatsUsers->Hosts->find('list', ['limit' => 200]);
        $chats = $this->ChatsUsers->Chats->find('list', ['limit' => 200]);
        $this->set(compact('chatsUser', 'users', 'visitors', 'hosts', 'chats'));
        $this->set('_serialize', ['chatsUser']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Chats User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $chatsUser = $this->ChatsUsers->get($id);
        if ($this->ChatsUsers->delete($chatsUser)) {
            $this->Flash->success(__('The chats user has been deleted.'));
        } else {
            $this->Flash->error(__('The chats user could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
