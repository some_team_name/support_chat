<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Messages Controller
 *
 * @property \App\Model\Table\MessagesTable $Messages
 */
class MessagesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        // TODO: if !isset $this->request->params['chat_id'] - throw http error
        if (!isset($this->request->params['chat_id'])) return;
        $chatId = (int) $this->request->params['chat_id'];
        $afterTime = $this->request->query('afterTime');

        $paginateCondition = ['Messages.chat_id' => $chatId];
        
        if (!empty($afterTime)) {
            $paginateCondition['Messages.created >'] = $afterTime;
        }

        $this->paginate = [
            'conditions' => $paginateCondition,
            'contain' => ['Users', 'Visitors', 'Chats']
        ];
        $messages = $this->paginate($this->Messages);

        $this->set(compact('messages'));
        $this->set('_serialize', ['messages']);
    }

    /**
     * View method
     *
     * @param string|null $id Message id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $message = $this->Messages->get($id, [
            'contain' => ['Users', 'Visitors', 'Chats']
        ]);

        $this->set('message', $message);
        $this->set('_serialize', ['message']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if (empty($this->request->params['chat_id'])) {
            // TODO: throw http error (have no chat_id)
            return;
        }

        $message = $this->Messages->newEntity($this->request->data);
        $message->chat_id = intval($this->request->params['chat_id']);

        $this->Messages->Chats->setMessageSender($message);

        
        if ($message->chat_id > 0 && $this->Messages->save($message)) {
            $resultMessage = __('The message has been saved.');
        } else {
            $resultMessage = __('The message could not be saved. Please, try again.');
        }

        $this->set([
            'resultMessage' => $resultMessage,
            'message' => $message,
            '_serialize' => ['message', 'resultMessage']
            ]);
    }

    /**
     * Edit method
     *
     * @param string|null $id Message id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $message = $this->Messages->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $message = $this->Messages->patchEntity($message, $this->request->data);
            if ($this->Messages->save($message)) {
                $this->Flash->success(__('The message has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The message could not be saved. Please, try again.'));
            }
        }
        $chats = $this->Messages->Chats->find('list', ['limit' => 200]);
        $this->set(compact('message', 'chats'));
        $this->set('_serialize', ['message']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Message id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $message = $this->Messages->get($id);
        if ($this->Messages->delete($message)) {
            $this->Flash->success(__('The message has been deleted.'));
        } else {
            $this->Flash->error(__('The message could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
