<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Chats Controller
 *
 * @property \App\Model\Table\ChatsTable $Chats
 */
class ChatsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        if (!empty($this->request->params['user_id'])) {
            $userId = (int) $this->request->params['user_id']; 
            $chats = $this->Chats->getUserChats($userId);
        } else {
            $this->paginate = [
                'contain' => ['Hosts']
            ];
            $chats = $this->paginate($this->Chats);
        }

        $this->set(compact('chats'));
        $this->set('_serialize', ['chats']);
    }

    /**
     * View method
     *
     * @param string|null $id Chat id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $chat = $this->Chats->get($id, [
            'contain' => ['Hosts', 'Users', 'Visitors', 'ChatSettings', 'Messages']
        ]);

        $this->set('chat', $chat);
        $this->set('_serialize', ['chat']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $chat = $this->Chats->newEntity($this->request->data);
        $this->set(compact('chat', 'hosts', 'users', 'visitors'));
        $this->Chats->setHostId($chat);

        if ($this->Chats->save($chat)) {
            $message = __('The chat has been saved.');
            if (empty($this->request->params['user_id'])) {
                $visitor = $this->Chats->Visitors->newEntity($this->request->data);
                $visitor->host_id = $chat->host_id;
                $this->Chats->Visitors->save($visitor);
                $this->Chats->Visitors->link($chat, [$visitor]);
            } else {
                $user = $this->Chats->Users->get($id);
                $this->Chats->Users->link($chat, [$user]);
            }
        } else {
            $message = __('The chat could not be saved. Please, try again.');
        }

        if (  !empty($chat->visitors) && !empty($chat->visitors[0]) && !empty($chat->visitors[0]->id) ) {
            $chat->visitor_id = $chat->visitors[0]->id;
            unset($chat->visitors);
        }
        
        $this->set([
            'message' => $message,
            'chat' => $chat,
            '_serialize' => ['message', 'chat']
        ]);
    }

    /**
     * Edit method
     *
     * @param string|null $id Chat id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $chat = $this->Chats->get($id, [
            'contain' => ['Users','Visitors']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $chat = $this->Chats->patchEntity($chat, $this->request->data);
            if ($this->Chats->save($chat)) {
                $this->Flash->success(__('The chat has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The chat could not be saved. Please, try again.'));
            }
        }
        $hosts = $this->Chats->Hosts->find('list', ['limit' => 200]);
        $users = $this->Chats->Users->find('list', ['limit' => 200]);
        $visitors = $this->Chats->Visitors->find('list', ['limit' => 200]);
        $this->set(compact('chat', 'hosts', 'users', 'visitors'));
        $this->set('_serialize', ['chat']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Chat id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $chat = $this->Chats->get($id);
        if ($this->Chats->delete($chat)) {
            $this->Flash->success(__('The chat has been deleted.'));
        } else {
            $this->Flash->error(__('The chat could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
