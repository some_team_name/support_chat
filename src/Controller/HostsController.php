<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Hosts Controller
 *
 * @property \App\Model\Table\HostsTable $Hosts
 */
class HostsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Accounts']
        ];
        $hosts = $this->paginate($this->Hosts);

        $this->set(compact('hosts'));
        $this->set('_serialize', ['hosts']);
    }

    /**
     * View method
     *
     * @param string|null $id Host id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $host = $this->Hosts->get($id, [
            'contain' => ['Accounts', 'Users', 'Chats', 'ChatsUsers', 'HostSettings', 'Visitors']
        ]);

        $this->set('host', $host);
        $this->set('_serialize', ['host']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $host = $this->Hosts->newEntity();
        $message = [];

        if ($this->request->is('post')) {
            $host = $this->Hosts->patchEntity($host, $this->request->data);

            $curUserId = $this->Auth->user('id');
            if (is_null($curUserId)) return; // TODO: throw an error

            $curUser = $this->Hosts->Accounts->Users->get($curUserId);
            if (!$curUser->isAdmin()) return; // TODO: throw an rights error
            $host->account_id = $curUser->account_id;
            $host->host = $this->Hosts->getURLHost($host->host);

            if ($this->Hosts->save($host)) {
                $message['success'] = __('The host has been saved.');

                $this->Flash->success($message['success']);
                if (!$this->request->is('ajax')) {
                    return $this->redirect(['action' => 'index']);
                }
            } else {
                $message['error'] = __('The host could not be saved. Please, try again.');

                $this->Flash->error($message['error']);
            }
        }
        $accounts = $this->Hosts->Accounts->find('list', ['limit' => 200]);
        $this->set(compact('host', 'accounts', 'message'));
        $this->set('_serialize', ['host', 'message']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Host id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $host = $this->Hosts->get($id, [
            'contain' => ['Users']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $host = $this->Hosts->patchEntity($host, $this->request->data);
            if ($this->Hosts->save($host)) {
                $this->Flash->success(__('The host has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The host could not be saved. Please, try again.'));
            }
        }
        $accounts = $this->Hosts->Accounts->find('list', ['limit' => 200]);
        $users = $this->Hosts->Users->find('list', ['limit' => 200]);
        $this->set(compact('host', 'accounts', 'users'));
        $this->set('_serialize', ['host']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Host id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $host = $this->Hosts->get($id);
        if ($this->Hosts->delete($host)) {
            $this->Flash->success(__('The host has been deleted.'));
        } else {
            $this->Flash->error(__('The host could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
