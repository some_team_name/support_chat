<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Event\Event;


/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController
{
    const ADMIN_CATEGORY = 'Admin';
    const ADMIN_PAGE = 'index';

    // public function initialize()
    // {
    //     parent::initialize();
    //     $this->Auth->allow(['display']);
    // }

    /**
     * Displays a view
     *
     * @return void|\Cake\Network\Response
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function display()
    {
        $path = func_get_args();

        $count = count($path);
        if (!$count) {
            return $this->redirect('/');
        }
        $page = $subpage = null;
        $this->viewBuilder()->layout('default');

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }
        if (is_null($subpage) && $page === self::ADMIN_CATEGORY) $subpage = self::ADMIN_PAGE;

        $this->set(compact('page', 'subpage'));

        if ($page === self::ADMIN_CATEGORY 
            && $subpage != 'display'
            && method_exists($this, $subpage)
            && $this->Auth->user('id') !== null // add auth redirect
        ) {
            $this->loadModel('Users');
            $user = $this->Users->get($this->Auth->user('id'), [
                'contain' => ['Accounts'=>['Hosts', 'Users']]
            ]);

            $this->set(compact('user'));
            $this->set('_serialize', ['user']);

            $this->viewBuilder()->layout('admin');
            $this->$subpage($user);
            return;
        } else if (is_null($this->Auth->user('id'))) {
            return $this->redirect(['controller' => 'Users', 'action' => 'login']);
        }

        try {
            $this->render(implode('/', $path));
        } catch (MissingTemplateException $e) {
            if (Configure::read('debug')) {
                throw $e;
            }
            throw new NotFoundException();
        }
    }

    public function index() {
        $this->renderMe(__FUNCTION__);
    }

    public function sites($user) {
        $this->loadModel('Hosts');

        // $hosts = $this->Hosts->find('all', [
        //     'contain' => ['Accounts.Users', 'Chats']
        // ]);

        $query = $this->Hosts->find()
         ->where(['account_id' => $user->account_id]);
        $hosts = $query
        // ->where(['Hosts.account_id' => $user->account_id])
        ->select(['activeChats' => $query->func()->count('Chats.id')])
        ->leftJoinWith('Chats', function ($q) {
            return $q->where(['Chats.status' => 'open']);
        })
        ->group(['Hosts.id'])
        ->autoFields(true);

        $this->set(compact('hosts'));
        $this->set('_serialize', ['hosts']);
        $this->renderMe(__FUNCTION__);
    }

    public function users($user) {
        $this->renderMe(__FUNCTION__);
    }

    public function site() {

    }

    private function renderMe($methodMe) {
        $this->render(self::ADMIN_CATEGORY . "/$methodMe");
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        // filter actions which should not output debug messages
        if ($this->request->params['pass'][0] === self::ADMIN_CATEGORY) {
            // if(in_array($this->action, array('export'))) {
                // Configure::write('debug', 0);
                // Plugin::unload('DebugKit.Toolbar');
            // }
        }
    }
}
