-- phpMyAdmin SQL Dump
-- version 4.2.3deb1.precise~ppa.1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Апр 25 2016 г., 03:29
-- Версия сервера: 5.5.49-0ubuntu0.12.04.1
-- Версия PHP: 5.4.45-2+deb.sury.org~precise+2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `chat`
--

-- --------------------------------------------------------

--
-- Структура таблицы `accounts`
--

CREATE TABLE IF NOT EXISTS `accounts` (
`id` int(11) NOT NULL,
  `balance` float DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=35 ;

--
-- Дамп данных таблицы `accounts`
--

INSERT INTO `accounts` (`id`, `balance`, `created`, `modified`) VALUES
(33, 0, '2016-03-07 21:58:33', '2016-03-07 21:58:33'),
(34, 0, '2016-03-08 11:17:29', '2016-03-08 11:17:29');

-- --------------------------------------------------------

--
-- Структура таблицы `chats`
--

CREATE TABLE IF NOT EXISTS `chats` (
`id` int(10) unsigned NOT NULL,
  `host_id` int(11) DEFAULT NULL,
  `status` enum('open','close','archive') NOT NULL DEFAULT 'open',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=32 ;

--
-- Дамп данных таблицы `chats`
--

INSERT INTO `chats` (`id`, `host_id`, `status`, `created`, `modified`) VALUES
(6, 1, 'open', '2016-03-08 11:22:30', '2016-03-08 11:22:30'),
(30, 1, 'open', '2016-03-20 19:27:55', '2016-03-20 19:27:55'),
(31, 1, 'open', '2016-03-20 22:08:34', '2016-03-20 22:08:34');

-- --------------------------------------------------------

--
-- Структура таблицы `chats_users`
--

CREATE TABLE IF NOT EXISTS `chats_users` (
`id` int(11) NOT NULL,
  `chat_id` int(10) unsigned NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `host_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `chats_visitors`
--

CREATE TABLE IF NOT EXISTS `chats_visitors` (
`id` int(11) NOT NULL,
  `visitor_id` int(11) DEFAULT NULL,
  `chat_id` int(10) unsigned NOT NULL,
  `host_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `chats_visitors`
--

INSERT INTO `chats_visitors` (`id`, `visitor_id`, `chat_id`, `host_id`, `created`, `modified`) VALUES
(1, 17, 30, NULL, '2016-03-20 19:27:55', '2016-03-20 19:27:55'),
(2, 18, 31, NULL, '2016-03-20 22:08:34', '2016-03-20 22:08:34');

-- --------------------------------------------------------

--
-- Структура таблицы `chat_settings`
--

CREATE TABLE IF NOT EXISTS `chat_settings` (
  `chat_id` int(10) unsigned NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `hosts`
--

CREATE TABLE IF NOT EXISTS `hosts` (
`id` int(11) NOT NULL,
  `host` varchar(2056) NOT NULL,
  `account_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `hosts`
--

INSERT INTO `hosts` (`id`, `host`, `account_id`, `created`, `modified`) VALUES
(1, 'dev-lem.ru', 33, '2016-03-07 22:15:37', '2016-03-07 22:15:37');

-- --------------------------------------------------------

--
-- Структура таблицы `host_settings`
--

CREATE TABLE IF NOT EXISTS `host_settings` (
`id` int(11) NOT NULL,
  `host_id` int(11) NOT NULL,
  `lang` varchar(255) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
`id` int(10) unsigned NOT NULL,
  `chat_user_id` int(11) DEFAULT NULL,
  `chat_visitor_id` int(11) DEFAULT NULL,
  `chat_id` int(10) unsigned NOT NULL,
  `body` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=49 ;

--
-- Дамп данных таблицы `messages`
--

INSERT INTO `messages` (`id`, `chat_user_id`, `chat_visitor_id`, `chat_id`, `body`, `created`, `modified`) VALUES
(37, 18, NULL, 6, 'mine', '2016-03-20 15:52:55', '2016-03-20 15:52:55'),
(38, 18, NULL, 6, 'now 18:17\n', '2016-03-20 16:17:38', '2016-03-20 16:17:38'),
(44, NULL, 17, 30, 'hi, i am eugene', '2016-03-20 19:28:08', '2016-03-20 19:28:08'),
(45, 18, NULL, 30, 'how are you?', '2016-03-20 19:28:45', '2016-03-20 19:28:45'),
(46, NULL, 18, 31, 'helllo\n', '2016-03-20 22:08:49', '2016-03-20 22:08:49'),
(47, 18, NULL, 31, 'how areyou?', '2016-03-20 22:09:11', '2016-03-20 22:09:11'),
(48, NULL, 18, 31, '<?php\necho ''5'';\n// 3, перемещать по экрану\n			Event.add(document, ''mousemove'', moveAt)\n\n			// 4. отследить окончание переноса\n			Event.add(self, ''mouseup'', removeHandlers);', '2016-03-20 22:10:59', '2016-03-20 22:10:59');

-- --------------------------------------------------------

--
-- Структура таблицы `profiles`
--

CREATE TABLE IF NOT EXISTS `profiles` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `account_id` int(11) NOT NULL,
  `account_owner` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `role`, `account_id`, `account_owner`, `created`, `modified`) VALUES
(18, 'lemaxe@gmail.com', '$2y$10$IcljxPWqUk2IHSaPdV4zkOAvdTdYHy5s2pqwPccjfWD3iVtYLZz3y', 'admin', 33, 1, '2016-03-07 21:58:33', '2016-03-07 21:58:33');

-- --------------------------------------------------------

--
-- Структура таблицы `visitors`
--

CREATE TABLE IF NOT EXISTS `visitors` (
`id` int(11) NOT NULL,
  `host_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Дамп данных таблицы `visitors`
--

INSERT INTO `visitors` (`id`, `host_id`, `created`, `modified`) VALUES
(17, 1, '2016-03-20 19:27:55', '2016-03-20 19:27:55'),
(18, 1, '2016-03-20 22:08:34', '2016-03-20 22:08:34');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chats`
--
ALTER TABLE `chats`
 ADD PRIMARY KEY (`id`), ADD KEY `host_key` (`host_id`);

--
-- Indexes for table `chats_users`
--
ALTER TABLE `chats_users`
 ADD PRIMARY KEY (`id`), ADD KEY `user_key` (`user_id`), ADD KEY `chat_key` (`chat_id`);

--
-- Indexes for table `chats_visitors`
--
ALTER TABLE `chats_visitors`
 ADD PRIMARY KEY (`id`), ADD KEY `visitor_key` (`visitor_id`), ADD KEY `chat_key` (`chat_id`);

--
-- Indexes for table `chat_settings`
--
ALTER TABLE `chat_settings`
 ADD PRIMARY KEY (`chat_id`);

--
-- Indexes for table `hosts`
--
ALTER TABLE `hosts`
 ADD PRIMARY KEY (`id`), ADD KEY `account_key` (`account_id`);

--
-- Indexes for table `host_settings`
--
ALTER TABLE `host_settings`
 ADD PRIMARY KEY (`id`), ADD KEY `host_key` (`host_id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
 ADD PRIMARY KEY (`id`), ADD KEY `chat_key` (`chat_id`), ADD KEY `messages_ibfk_3` (`chat_visitor_id`), ADD KEY `messages_ibfk_2` (`chat_user_id`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
 ADD PRIMARY KEY (`id`), ADD KEY `user_key` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD KEY `account_key` (`account_id`);

--
-- Indexes for table `visitors`
--
ALTER TABLE `visitors`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `chats`
--
ALTER TABLE `chats`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `chats_users`
--
ALTER TABLE `chats_users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `chats_visitors`
--
ALTER TABLE `chats_visitors`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `hosts`
--
ALTER TABLE `hosts`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `host_settings`
--
ALTER TABLE `host_settings`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `visitors`
--
ALTER TABLE `visitors`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `chats`
--
ALTER TABLE `chats`
ADD CONSTRAINT `chats_ibfk_1` FOREIGN KEY (`host_id`) REFERENCES `hosts` (`id`);

--
-- Ограничения внешнего ключа таблицы `chats_users`
--
ALTER TABLE `chats_users`
ADD CONSTRAINT `chats_users_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
ADD CONSTRAINT `chats_users_ibfk_3` FOREIGN KEY (`chat_id`) REFERENCES `chats` (`id`);

--
-- Ограничения внешнего ключа таблицы `chats_visitors`
--
ALTER TABLE `chats_visitors`
ADD CONSTRAINT `chats_visitors_ibfk_1` FOREIGN KEY (`visitor_id`) REFERENCES `visitors` (`id`),
ADD CONSTRAINT `chats_visitors_ibfk_2` FOREIGN KEY (`chat_id`) REFERENCES `chats` (`id`);

--
-- Ограничения внешнего ключа таблицы `chat_settings`
--
ALTER TABLE `chat_settings`
ADD CONSTRAINT `chat_settings_ibfk_1` FOREIGN KEY (`chat_id`) REFERENCES `chats` (`id`);

--
-- Ограничения внешнего ключа таблицы `hosts`
--
ALTER TABLE `hosts`
ADD CONSTRAINT `hosts_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`);

--
-- Ограничения внешнего ключа таблицы `host_settings`
--
ALTER TABLE `host_settings`
ADD CONSTRAINT `host_settings_ibfk_1` FOREIGN KEY (`host_id`) REFERENCES `hosts` (`id`);

--
-- Ограничения внешнего ключа таблицы `messages`
--
ALTER TABLE `messages`
ADD CONSTRAINT `messages_ibfk_1` FOREIGN KEY (`chat_id`) REFERENCES `chats` (`id`),
ADD CONSTRAINT `messages_ibfk_2` FOREIGN KEY (`chat_user_id`) REFERENCES `users` (`id`),
ADD CONSTRAINT `messages_ibfk_3` FOREIGN KEY (`chat_visitor_id`) REFERENCES `visitors` (`id`);

--
-- Ограничения внешнего ключа таблицы `profiles`
--
ALTER TABLE `profiles`
ADD CONSTRAINT `profiles_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ограничения внешнего ключа таблицы `users`
--
ALTER TABLE `users`
ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
