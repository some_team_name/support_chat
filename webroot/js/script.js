$(document).ready(function(){
	$('.checkbox_square-green, .checkbox.i-checks label').on('click', function(e) {
		var $checkbox = $(this).find('input[type=checkbox]');

		if ($checkbox.prop('checked')) {
			$checkbox.parent().addClass('checked');
		} else {
			$checkbox.parent().removeClass('checked');
		}
	});

	$('form#add-user .save-btn').on('click', function(e) {
		var $form = $(this).parents('form');
		var data = $form.serialize();

		$.post($form.attr('action'), data, function(response) {

			if ("undefined" !== typeof response.message.success) {
				alert(response.message.success);
				$('#modal-message').modal('hide');
				addNewUserRow(response.user);
			} else {
				alert(response.message.error)
			}
		}, 'json');

	});

	$('.site-add-form .save-btn').on('click', function(e) {
		e.preventDefault();

		var $form = $(this).parents('form');
		var data = $form.serialize();

		$.post($form.attr('action'), data, function(response) {

			if ("undefined" !== typeof response.message.success) {
				alert(response.message.success);
				$('#modal-message').modal('hide');
				addNewHostRow(response.host);
			} else {
				alert(response.message.error)
			}
		}, 'json');

	});
});

function addNewUserRow(user) {
	var $table = $('.users-list table');
	var $rows = $table.find('thead th');
	var $newRow = $('<tr>');

	$rows.each(function(i, el) {
		var type = $(el).data('col-type');
		var $col = $('<td>');
		var value = '';

		if (type == 'number') {
			$col.text($table.find('tbody tr').length + 1);
		} else if (user.hasOwnProperty(type)) {
			$col.text(user[type]);
		} else if (type == 'actions') {
			$col.append( $('<i>').addClass('fa fa-pencil-square-o'), $('<i>').addClass('fa fa-trash') )
		} else {
			$col.text(type);
		}
		$newRow.append($col);
	});

	$table.find('tbody').append($newRow);
	incrementMyValue($('.users-counter'));
}
function addNewHostRow(host) {
	var $table = $('.sites-list table');
	var $rows = $table.find('thead th');
	var $newRow = $('<tr>');

	$rows.each(function(i, el) {
		var type = $(el).data('col-type');
		var $col = $('<td>');
		var value = '';

		if (type == 'number') {
			$col.text($table.find('tbody tr').length + 1);
		} else if (host.hasOwnProperty(type)) {
			$col.text(host[type]);
		} else if (type == 'actions') {
			$col.append( $('<i>').addClass('fa fa-pencil-square-o'), $('<i>').addClass('fa fa-trash') )
		} else {
			$col.text(type);
		}
		$newRow.append($col);
	});

	$table.find('tbody').append($newRow);
	incrementMyValue($('.hosts-counter'));
	addHost2SideBar(host);
}

function incrementMyValue($me) {
	$me.text( parseInt($me.text()) + 1 );
}

function addHost2SideBar(host) {
	var $menu = $('.menu-hosts ul.sub-menu');
	var sampleHref = $menu.find('.sites-list a').attr('href').split('/sites').join('/site/');
	var $a = $('<a>').attr('href', sampleHref + host.id).text(host.host);
	var $li = $('<li>').append($a);
	$menu.append($li);
}
