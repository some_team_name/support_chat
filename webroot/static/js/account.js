"use strict";
var dev = true;
var devUserId = 18;

var Event = (function() {
  // текущий номер обработчика
  var guid = 0;

  function fixEvent(event) {
    // кросс-браузерная предобработка объекта-события
    // нормализация свойств и т.п.
    // получить объект события
	event = event || window.event;

	// один объект события может передаваться по цепочке разным обработчикам
	// при этом кроссбраузерная обработка будет вызвана только 1 раз
	if ( event.isFixed ) {
		return event;
	}
	event.isFixed = true; // пометить событие как обработанное

	// добавить preventDefault/stopPropagation для IE
	event.preventDefault = event.preventDefault || function(){this.returnValue = false};
	event.stopPropagation = event.stopPropagaton || function(){this.cancelBubble = true};

	// добавить target для IE
	if (!event.target) {
	  event.target = event.srcElement;
	}

	// добавить relatedTarget в IE, если это нужно
	if (!event.relatedTarget && event.fromElement) {
	  event.relatedTarget = event.fromElement == event.target ? event.toElement : event.fromElement;
	}

	// вычислить pageX/pageY для IE
	if ( event.pageX == null && event.clientX != null ) {
	  var html = document.documentElement, body = document.body;
	  event.pageX = event.clientX + (html && html.scrollLeft || body && body.scrollLeft || 0) - (html.clientLeft || 0);
	  event.pageY = event.clientY + (html && html.scrollTop || body && body.scrollTop || 0) - (html.clientTop || 0);
	}

	// записать нажатую кнопку мыши в which для IE
	// 1 == левая; 2 == средняя; 3 == правая
	if ( !event.which && event.button ) {
	  event.which = (event.button & 1 ? 1 : ( event.button & 2 ? 3 : ( event.button & 4 ? 2 : 0 ) ));
	}

	return event;
  }

  function commonHandler(event) {
    // вспомогательный универсальный обработчик 
	event = fixEvent(event);
	var handlers = this.events[event.type];

	var errors = [];

	for ( var g in handlers ) {
		try {
			var ret = handlers[g].call(this, event);
			if ( ret === false ) {
			    event.preventDefault();
			    event.stopPropagation();
			}

			if (event.stopNow) break;
		}  catch(e) {
			errors.push(e);
		}
	}

	if (errors.length == 1) {
		throw errors[0];
	} else if (errors.length > 1) {
		var e = new Error("Multiple errors thrown in handling 'sig', see errors property");
		e.errors = errors;
		throw e;
	} 
  }
  
  return {
    add: function(elem, type, handler) {
      // добавить обработчик события
	  // исправляем небольшой глюк IE с передачей объекта window
	  if (elem.setInterval && ( elem != window && !elem.frameElement ) ) {
		elem = window;
	  }
	  
	  if (!handler.guid) {
	    handler.guid = ++guid;
	  }
	  
	  if (!elem.events) {
		elem.events = {};
		
		elem.handle = function(event) {
		  if (typeof Event !== "undefined") {
			return commonHandler.call(elem, event);
		  }
		}
	  }
	  
	  if (!elem.events[type]) {
	    elem.events[type] = {};
	  
	    if (elem.addEventListener)
	      elem.addEventListener(type, elem.handle, false);
	    else if (elem.attachEvent)
	      elem.attachEvent("on" + type, elem.handle);
	  }
	  
	  elem.events[type][handler.guid] = handler;
    },
    
    remove: function(elem, type, handler) {
		// удалить обработчик события
		var handlers = elem.events && elem.events[type];

		if (!handlers) return;

		if (!handler) {
			for ( var handle in handlers ) {
				delete events[type][handle];
			}
			return;
		}
	  
		delete handlers[handler.guid];

		for(var any in handlers) return;

		if (elem.removeEventListener)
			elem.removeEventListener(type, elem.handle, false);
		else if (elem.detachEvent)
			elem.detachEvent("on" + type, elem.handle);
	    
		delete elem.events[type];

		for (var any in elem.events) return;
		try {
			delete elem.handle;
			delete elem.events;
		} catch(e) { // IE
			elem.removeAttribute("handle");
			elem.removeAttribute("events");
		}
    }
  }
}());

var AJAX = (function() {

	function createXHR(type, url) {
		var xhr = new XMLHttpRequest();
		// xhr.withCredentials = true
		xhr.open(type, url, true);
		return xhr;
	}

	function setHeaders(xhr) {
		// xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
		xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded; charset=UTF-8');
		xhr.setRequestHeader('Accept', 'application/json, text/javascript, */*; q=0.01');
	}

	function onSuccess(xhr, callback, raw) {
		xhr.onreadystatechange = function() {
		  if (this.readyState != 4) return;

		  // по окончании запроса доступны:
		  // status, statusText
		  // responseText, responseXML (при content-type: text/xml)

		  if (this.status != 200) {
		    // обработать ошибку
		    console.log( 'ошибка: ' + (this.status ? this.statusText : 'запрос не удался') );
		    return;
		  }

		  // получить результат из this.responseText или this.responseXML
		  var result = '';
		  if (raw) result = this.responseText;
		  else result = JSON.parse(this.responseText);

		  callback(result);
		}
	}

	function ajaxRequest(type, url, data, callback, raw) {
		var xhr = createXHR(type, url);
		setHeaders(xhr);
		if (data === null) xhr.send();
		else xhr.send(JSON.stringify(data));
		onSuccess(xhr, callback, raw);
		return xhr;
	}

	return {
		get: function(url, callback, raw) {
			//index
			//view
			return ajaxRequest('GET', url, null, callback, raw);

		},
		post: function(url, data, callback, raw) {
			//add
			//  'http://dev-lem.ru/proj/chat/chats'
			return ajaxRequest('POST', url, data, callback, raw);
		},
		patch: function(url, data, callback, raw) {
			// edit
			return ajaxRequest('PATCH', url, data, callback, raw);
		},
		delete: function(url, data, callback, raw) {
			//delete
			return ajaxRequest('DELETE', url, data, callback, raw);
		}
	}
}());

function bindReady(handler){

	var called = false;

	function ready() {
		if (called) return; // for call once
		called = true;
		handler();
	}

	if ( document.addEventListener ) {
		document.addEventListener( "DOMContentLoaded", function(){
			ready();
		}, false )
	} else if ( document.attachEvent ) {

		if ( document.documentElement.doScroll && window == window.top ) {
			function tryScroll(){
				if (called) return;
				if (!document.body) return;
				try {
					document.documentElement.doScroll("left");
					ready();
				} catch(e) {
					setTimeout(tryScroll, 0);
				}
			}
			tryScroll();
		}

		document.attachEvent("onreadystatechange", function(){

			if ( document.readyState === "complete" ) {
				ready();
			}
		});
	}

    if (window.addEventListener)
        window.addEventListener('load', ready, false);
    else if (window.attachEvent)
        window.attachEvent('onload', ready);
    /*  else  // (4.1)
        window.onload=ready
	*/
}


var readyList = [];

function onReady(handler) {

	if (!readyList.length) {
		bindReady(function() {
			for(var i=0; i<readyList.length; i++) {
				readyList[i]();
			}
		});
	}

	readyList.push(handler);
}

function domObj(domItem) {
	if ( !(isElement(domItem)) && !(isNode(domItem)) ) {
		if ("string" === typeof domItem) {
			var span = document.createElement('span');
			span.innerHTML = domItem;
			domItem = span.children[0];
			span = undefined;
		} else return;
	}

	//Returns true if it is a DOM node
	function isNode(o){
		return (
			typeof Node === "object" ? o instanceof Node : 
			o && typeof o === "object" && typeof o.nodeType === "number" && typeof o.nodeName==="string"
		);
	}

	//Returns true if it is a DOM element    
	function isElement(o){
		return (
			typeof HTMLElement === "object" ? o instanceof HTMLElement : //DOM2
			o && typeof o === "object" && o !== null && o.nodeType === 1 && typeof o.nodeName==="string"
		);
	}

	function styles2Obj(styleStr) {
		var result = {};
		if ("string" !== typeof styleStr) return result;
		
		var styles = styleStr.trim().split(';');
		var style = ''; 

		for (var i = 0; i < styles.length; i++) {
			style = styles[i].trim();

			if (style.length === 0) continue;
			if (style.indexOf(':') === -1) continue;

			var styleArr = style.split(':');
			result[styleArr[0]] = styleArr.slice(1).join(':').trim();
		}
		return result;
	}

	function styles2Str(styleObj) {
		var result = [];
		for(var i in styleObj) {
			result.push(i + ': ' + styleObj[i].trim());
		}

		return result.join('; ');
	}


	function obj() {

	}

	function setStyle(name, value) {
		var styles = styles2Obj(obj.attr('style'));
		styles[name] = value;
		obj.attr('style', styles2Str(styles));
	}

	function getStyle(name) {
		var styles = styles2Obj(obj.attr('style'));
		return styles[name];
	}

	obj.attr = function(name, value) {
		if ("undefined" !== typeof value) return domItem.setAttribute(name, value);
		return domItem.getAttribute(name);
	}

	obj.css = function(options, value) {
		if ("undefined" === typeof options) {
			return obj.attr('style');
		} else if ("object" === typeof options) {
			for (var name in options) {
				setStyle(name, options[name]);
			}
		} else {
			if ("undefined" === typeof value) return getStyle(options); // get style
			// set style
			setStyle(options, value);
		}
	}

	obj.hide = function() {
		obj.css('display', 'none');
	}

	obj.show = function() {
		obj.css('display', 'block');
	}

	obj.item = domItem;

	return obj;
};


function Chat(id, userType, userIdVal) {
	// текущий номер обработчика
	var guid = 0;
	var self = this;
	var visitorId = null, userId = null, status;
	var $btn = {}, $chat = {}, $closeBtn = {}, $messTextField = {}, $messBlock = {};

	this.lastMessageTime = 0;
	this.chatId = null;

	// TODO: add object properties array

	var updateTimeoutId = 0, updateAborted = false;

	if ("undefined" === typeof userType) userType = 'visitor';

	if ("object" === typeof id) {
		var chatObj = id;
		self.chatId = chatObj.id;
		status = chatObj.status;
	} else if ("number" === typeof id) {
		self.chatId = id;
	} else {
		//TODO: create chat. - now all working good
	}

	if (userId === null || "undefined" !== typeof userIdVal) {
		userId = userIdVal;
	}


	function attachSenders() {
		var $sendBtn = domObj($chat.item.getElementsByClassName('sh_enter_ico')[0]);

		if (!$messTextField.item) {
			var messConteiner = $chat.item.getElementsByClassName('sh_textarea_answer')[0];
			$messTextField = domObj(messConteiner.getElementsByTagName('textarea')[0]);
		}

		Event.add($sendBtn.item, 'click', function(event) {
			event.preventDefault();
			sendMessage();
		});

		Event.add($messTextField.item, 'keydown', function(e) {
			if (e.ctrlKey && e.keyCode == 13) {
				e.preventDefault();
				sendMessage();
			}
		});
	}

	function sendMessage() {
		var messVal = $messTextField.item.value;
		if (messVal.trim() === '') return;

		self.send($messTextField.item.value);
		$messTextField.item.value = '';
	}

	function isDateBigger(date1, date2) {
		return new Date(date1) < new Date(date2);
	}

	function addDragDrop() {
		var $header = $chat.item.getElementsByClassName('sh_header')[0];

		Event.add($header, 'mousedown', function(event) {
			if (event.target.className !== $header.className) return;
			var self = this;
			var parent = self.parentElement;

			// 2. разместить на том же месте, но в абсолютных координатах
			parent.style.position = 'absolute';
			moveAt(event);

			parent.style.zIndex = 1000; // показывать мяч над другими элементами

			// передвинуть мяч под координаты курсора
			// и сдвинуть на половину ширины/высоты для центрирования
			function moveAt(e) {
				e.cancelBubble = true;
				parent.style.left = e.pageX - self.offsetWidth / 2 + 'px';
				parent.style.top = e.pageY - self.offsetHeight / 2 + 'px';
			}

			function removeHandlers(e) {
				Event.remove(document, 'mousemove', moveAt);
				Event.remove(self, 'mouseup', removeHandlers);
			}

			// 3, перемещать по экрану
			Event.add(document, 'mousemove', moveAt)

			// 4. отследить окончание переноса
			Event.add(self, 'mouseup', removeHandlers);

		});
	}

	this.createView = function() {
		if (self.chatTemplate === null) {
			Chat.prototype.afterViewLoaded.push(self.createView);
			return;
			 // throw new Error("Chat template do not loaded");// TODO: add messages translating
		}

		$btn = domObj(self.chatTemplate.btn);
		var htmlViewId = '_' + self.chatId;
		$chat = domObj(self.chatTemplate.chat);

		var btnViewId = $btn.attr('id');
		$btn.attr('id', (btnViewId ? btnViewId : 'btn_view') + htmlViewId);
		var chatViewId = $chat.attr('id');
		$chat.attr('id', (chatViewId ? chatViewId : 'chat_view') + htmlViewId);

		document.body.appendChild($btn.item);
		document.body.appendChild($chat.item);
		$closeBtn = domObj($chat.item.getElementsByClassName('sh_hico_close')[0]);
		$messBlock = domObj($chat.item.getElementsByClassName('sh_messages')[0]);

		attachSenders();
		addDragDrop();

		Event.add($btn.item, 'click', function(event) {
			event.preventDefault();
			updateAborted = false;

			if (!visitorId && !userId) {
				AJAX.post(self.chatAPIUrl + 'chats', {}, function(response) {

					chatId = response.chat.id;

					if ("undefined" !== typeof response.chat.visitor_id) {
						visitorId = response.chat.visitor_id;
					}

					self.showChatBlock();
				});
			} else {
				self.showChatBlock();
			}
		});

		Event.add($closeBtn.item, 'click', function(event) {
			event.preventDefault();
			self.stopMessagesUpdate();

			// hide chat
			$chat.hide();
			$btn.show();
		});
	}

	this.showChatBlock = function() {
		self.updateMessages();

		// show chat
		$chat.show();
		$btn.hide();
	}

	this.send = function(message) {
		console.log(self.chatId, visitorId, userId, message);
		var data = {'body':message};

		if (self.amIVisitor()) data.visitor_id = visitorId;
		else if (self.amIUser()) data.user_id = userId;

		AJAX.post(self.chatAPIUrl + 'chats/'+self.chatId+'/messages', data, function(response) {
			debugger;
		});
	}

	this.setStatus = function(chatStatus) {
		status = chatStatus;
	}

	this.updateMessages = function() {
		console.log(self.chatId);
		if (updateAborted) return;

		AJAX.get(self.chatAPIUrl + 'chats/'+self.chatId+'/messages?afterTime=' + self.lastMessageTime, function(response) {
			if (updateAborted) return;

			updateTimeoutId = setTimeout(self.updateMessages, self.updateIntervalMsecs);

			for (var i in response.messages) {
				self.showMessage(response.messages[i]);
			}
		});
	}

	this.showMessage = function(message) {
		if (isDateBigger(self.lastMessageTime, message.created)) {
			self.lastMessageTime = message.created;
			if (self.isMyMessage(message)) {
				self.showMyMessage(message); // at the right side
			} else {
				self.showOthersMessage(message); // at the left side
			}
		}
	}


	this.stopMessagesUpdate = function() {
		console.log(self.chatId, updateTimeoutId);
		clearTimeout(updateTimeoutId);
		updateAborted = true;
	}

	this.isMyMessage = function(message) {
		if (self.amIVisitor()) { // visitor sends messages
			return message.chat_visitor_id === visitorId;
		} else if (self.amIUser()) { // user sends messages
			return message.chat_user_id === userId;
		}

		throw new Error("visitorId and userId is null - can`t detect who you are");// TODO: add messages translating
	}

	this.amIUser = function() {
		return null !== userId;
	}

	this.amIVisitor = function() {
		return null !== visitorId;
	}

	this.showMyMessage = function(message) {
		console.log('right side message: ' + message.body);
		self.fillMessage(message, 'You'); //TODO: add translating
		// you
	}
	this.showOthersMessage = function(message) {
		console.log('left side message: ' + message.body);
		var who = message.chat_visitor_id === null ? 'User ' + message.chat_user_id : 'Visitor ' + message.chat_visitor_id;

		self.fillMessage(message, who); //TODO: add translating
	}

	this.fillMessage = function(message, who) {
		var item = this.messItemTemplate.cloneNode(true);

		item.getElementsByClassName('sh_mess_time')[0].textContent = self.getDateMinutes(message.created);
		item.getElementsByClassName('sh_mess_nick')[0].textContent = who;
		item.getElementsByClassName('sh_mess_text')[0].textContent = message.body.split("\n").join("<br>");

		$messBlock.item.appendChild(item);
		return item;
	}

	this.getDateMinutes = function(dateStr) {
		var date = new Date(dateStr);
		return date.getHours() + ':' + date.getMinutes();
	}

	this.createView();

}

Chat.prepare = function() {
	function loadView(self) {
		if (self.prototype.chatTemplate !== null) return;

		AJAX.get(self.prototype.chatAPIUrl + 'static/html/chat_template.html', function(data) {
			if (self.prototype.chatTemplate !== null) {
				console.log('Chat templates already downloaded');
				return false;
			}

			var templates = data.trim().split('\n\n');
			var chatDom = domObj(templates[1]);
			self.prototype.messItemTemplate = chatDom.item.getElementsByClassName('sh_message')[0];
			self.prototype.messItemTemplate.parentElement.removeChild(self.prototype.messItemTemplate);
			self.prototype.chatTemplate = {'btn':templates[0], 'chat':chatDom.item.outerHTML};
			chatDom = undefined;
			callCallbacks();
		}, true); // get raw html
	}

	function callCallbacks() {
		var calbacks = Chat.prototype.afterViewLoaded;
		for (var i in calbacks) {
			calbacks[i]();
			delete Chat.prototype.afterViewLoaded[i];
		}
	}

	loadView(this);
}
Chat.prototype.chatTemplate = null;
Chat.prototype.messItemTemplate = null;
Chat.prototype.updateIntervalMsecs = 3000;
Chat.prototype.chatAPIUrl = 'http://dev-lem.ru/proj/chat/';
Chat.prototype.afterViewLoaded = [];

Chat.prepare();


var Chats = (function() {
	if (dev) {
		var userId = devUserId;
	}
	var chatList = {};

	return {
		init: function() {
			this.list();
		},
		list: function() {
			if ("undefined" === typeof userId) throw new Error("Have no userId"); // TODO: add messages translating
			var self = this;

			AJAX.get(Chat.prototype.chatAPIUrl + 'users/'+userId+'/chats', function(response) {
				for (var i in response.chats) {
					var chatObj = response.chats[i];
					if ("undefined" === typeof chatList[chatObj.id]) {
						chatObj.messages = [];
						chatList[chatObj.id] =  new Chat(chatObj, 'user', userId);
						chatList[chatObj.id].showChatBlock();
					} else {
						chatList[chatObj.id].setStatus(chatObj.status);
					}
				}
				
				self.update();
				// debugger;
			});

		},
		update: function() {
			var self = this;
			setTimeout(function() {self.list();}, 5000);
		}
	}

}());

/* */
onReady(function() {
	Chats.init();
});
